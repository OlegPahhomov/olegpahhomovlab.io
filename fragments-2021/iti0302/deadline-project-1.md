| part | deadline      | date        | notes  |
| ------------- | ------------- |:-------------:| -----|
| 0.1 | on time | **Saturday, 18.09 23:59** |  |
| 0.2 | on time | **Saturday, 02.10 23:59** |  |
| 1 | early bird | **Saturday, 16.10 23:59** | extra points |
| 1 | on time | **Saturday, 23.10 23:59** | each hour late is -2% rounded upwards |
| 1 | late | **Saturday, 30.10 23:59** | 1 week late -30% |
