| deadline      | date        | notes  |
| ------------- |-------------| -----|
| early bird | **Saturday, 13.11 23:59** | extra points |
| on time | **Saturday, 20.11 23:59** | each hour late is -2% rounded upwards |
| late | **Saturday, 27.11 23:59** | 1 week late -30% |
