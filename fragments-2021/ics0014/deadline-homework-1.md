| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| on time | Monday, **01.03 23:59** | for project part 1 early birds & on time |
| late | Monday, **08.03 23:59** | for project part 1 late |
