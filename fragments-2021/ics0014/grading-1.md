* [The grading excel](https://docs.google.com/spreadsheets/d/1EOqbHgg3hStWmqlY8xkJBci_33Rn4e0kxt42nUTNQ1c/edit?usp=sharing)
* Deduction + extras system
    * Deduction is designed to reduce max points from 100% to 90% (5 -> 4)
    * These points can be regained by doing different extras
    * Extras give you opportunity to explore different topics
* We will grade main/master branch only
    * [master -> main](../../git/main)
* Each team gets individual feedback
    * points + comments
* Small & beautiful is better than big and messy
