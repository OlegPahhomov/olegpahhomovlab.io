| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | **Saturday 11.12 23:59** | extra points |
| christmas bird | **Thursday 23.12 23:59** | 50% of extra points |
| on time | **Saturday 01.01 23:59** | each hour late is -2% rounded upwards |
| late | **Saturday 08.01 23:59** | 1 week late -30% |
