* To defend 2 things must happen:
    1) you submitting
    2) us grading
* After that registration doodle will be sent to your team chat
* Defences
    * Are done online in University Teams
    * Every team member must participate
    * Defences last ~15 minutes (longer with 4 ppl teams and shorter with 2 ppl teams)
* General defence guidelines
    * Start 5 minutes early to prepare your team and environments.
        * Everybody is present with sound and video
        * You can share screen
        * Your environments (front-end, back-end, database are up)
    * Demo. You will show your working application (not code). Learn to describe what you've done.
* After your short 2-3 min presentation we will ask you some questions about the application or code.
    * We will discuss your points so look them over and have some questions
