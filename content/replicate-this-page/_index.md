+++
title = "Replicate this page"
description = ""
weight = 17000
+++

{{< lead >}}
Replicating this page is not related to the course

This page was built using gitlab pages <a href="https://about.gitlab.com/stages-devops-lifecycle/pages/" target="_blank">Gitlab Pages</a> 
You can also use github pages <a href="https://pages.github.com/" target="_blank">Github Pages</a>  
This can be done by using gitlab -> new -> create from template -> <a href="https://gitlab.com/pages/hugo" target="_blank">Pages/Hugo</a>  
After that a different theme was selected. <a href="https://themes.gohugo.io/" target="_blank">Hugo themes</a>  

This page is using <a href="https://themes.gohugo.io/ace-documentation/" target="_blank">Ace documentation</a> theme. 
Click here for <a href="https://github.com/vantagedesign/ace-documentation" target="_blank">Github</a>.
Click here for <a href="https://docs.vantage-design.com/ace/" target="_blank">Official Demo</a> 

Ace is a theme for <a href="https://gohugo.io" target="_blank">Hugo</a>, a fast static website generator written in Go, that allows you to easily write well organized and clean documentation for your projects. It's as easy as writing your content in Markdown, running Hugo to generate static HTML, CSS and Javascript files and deploying those to your web server.
{{< /lead >}}
