+++
title = "Installing hugo"
description = ""
weight = 20000
+++

Hugo is used for hugo version of gitlab-pages.

Installing:
https://gohugo.io/getting-started/installing/

For linux you will need to install brew first.
https://brew.sh/
brew path problem: https://github.com/Homebrew/brew/issues/6033

After you install it you can run it locally
```cmd
hugo server
```