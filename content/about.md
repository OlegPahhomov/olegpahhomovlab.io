+++
title = "Hi! My name is Oleg Pahhomov"
description = ""
+++

<img src="../OlegPahhomov.jpg" height="300">



I am a self-taught programmer. _I studied electrical engineering._ I wrote my first code in 2014. It was python. Same year I got into summer school and started working full time.  
It has been an incredible journey ever since. I have written a lot of code, done dozens of interesting (and not so interesting) projects, worked with different teams. 
I have drank a lot of coffee. Have I mentioned all the bugs and surprises?

I work as a software solutions architect. My work involves coding, code reviewing, mentoring, planning, envisioning products. 

In the year 2019 I joined TalTech as a part-time teacher. I am thankful for meaningful contribution I can make. In addition, I learn every day. 
If you're thinking of teaching part-time. Yes! It is amazing. It is amazing to contribute.


Teaching philosophy? I am a practical guy. I get things done. I believe in teaching the #greating and #latest.
  
Join me in the autumn;
- ITI0203 Information Systems in Java 
- ICS0024 Automated Testing

or in the spring
- ICS0014 Java Technologies


___
<img src="../Oracle-Certification-badge_OC-Associate600X600.png" alt-text="Oracle Associate" height="200">
<img src="../Oracle-Certification-badge_OC-Professional600X600.png" alt-text="Oracle Professional" height="200">

* [Oracle Certified Professional, Java SE 8 Programmer](https://www.youracclaim.com/badges/137d9c0a-27b6-4ed6-a2f3-29b80c6c9777)
* [Oracle Certified Associate, Java SE 8 Programmer](https://www.youracclaim.com/badges/7c000627-d74a-4f57-ba22-bc283318913b)
* [Oracle Certified Professional, Java SE 7 Programmer](https://www.youracclaim.com/badges/191013cb-24f5-4714-9e45-d1c56ef2ec20)
* [Oracle Certified Associate, Java SE 7 Programmer](https://www.youracclaim.com/badges/d976ffcf-d1b9-4d17-bbaa-7d90e6a64e1e)
