+++
title = "Licence"
description = ""
weight = 16500
+++

All content is **CC BY-NC**  
Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)

You are free to share and adopt when you give appropriate credit and use it for non-commercial purposes

https://creativecommons.org/licenses/by-nc/4.0/
