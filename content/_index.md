+++ title = "OP courses and guides"
description = ""
+++

{{< lead >}} Start your journey into IT with [Oleg Pahhomov](about) in partnership with TalTech.  
Pick your course(s). Here are multiple guides to support your journey. {{< /lead >}}

NB! ICS0014 2022 is my last and 10th course.

<div class="row py-3 mb-5">
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fab fa-java fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
				    <a href="ics0014-2022">
					ICS0014 Java Technologies
					</a>
				</h5>
				<p class="card-text text-muted">
				    Introduction to Java and git. Write your first program! Write tests with Junit. 
				    Work in teams. Write your first API with Spring Boot. 
				</p>
			</div>
		</div>
	</div>	
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-tachometer-alt fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					<a href="/pastcourses/iti0302-2021/">
					ITI0302 Web Application Project
					</a> 
				</h5>
				<p class="card-text text-muted">
				    Building modern application using Java, Spring Boot and Angular (react/vue). 
				    Setting up production and CD/CI pipeline.
				</p>
			</div>
		</div>
	</div>
    <div class="col-md-4">
		<div class="card flex-row border-0">
			<div class="mt-3">
				<span class="fas fa-cogs fa-2x text-primary"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
				    <a href="/pastcourses/ics0024-2021">
					ICS0024 Automated testing
				    </a>
				</h5>
				<p class="card-text text-muted">
				    Automated testing or testing automation? Developing with TDD. Setting up pipelines. 
				    UI tests with Selenium and cypress.
				</p>
			</div>
		</div>
	</div>
    
</div>

