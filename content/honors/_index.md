+++
title = "Honorary mentions"
description = ""
weight = 16000
+++

## ITI0302 2021 mentors
Andreas Saltsberg  
Gleb Komissarov  
Ilja Samoilov  
Kristjan Variksoo  
Lilia Tünts  
Nele Sergejeva  
Tavo Annus  
Valeri Andrejev  

## ITI0203 2020 mentors
Enrico Vompa  
Ilja Samoilov  
Marten Jürgenson  
Oskar Pihlak  
Tanel Ehatamm  
Tavo Annus  
Timo Loomets  


## ITI0203 2019 mentors
Ilja Samoilov  


{{< childpages >}}
