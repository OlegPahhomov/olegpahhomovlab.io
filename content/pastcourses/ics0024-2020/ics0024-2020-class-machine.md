+++
title = "ICS0024 Class Machine"
description = ""
weight = 200
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)

{{< alert style="warning" >}} This guide is intentionally minimalistic. For it is your goal to write a proper guide yourself. {{< /alert >}}

This guide is intentionally minimalistic.
For it is your goal to write a proper guide yourself.

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

## Initial setup
You have obtained the machine.  
I am using ubuntu 20.04  
All commands are done on the server


## connecting 
ssh -i ".ssh/olegoleg-t480s.pem" ubuntu@ec2-13-49-159-43.eu-north-1.compute.amazonaws.com
ssh ubuntu@ec2-13-49-159-43.eu-north-1.compute.amazonaws.com


## virtual memory
Aka we can survive angular build

Running htop will show how much memory you have.
https://itsfoss.com/create-swap-file-linux/  
For 2Gb virtual memory
```bash
sudo fallocate -l 2G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  
sudo swapon -show  
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
```

htop to check restart server and check whether virtual memory change was permanent sudo reboot

## install dependencies
sudo apt-get update  
sudo apt-get upgrade  
sudo apt-get install openjdk-11-jre openjdk-11-jdk  

## gitlab runner
use class guide  
use amd64  
comment out some gitlab runner file
https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26605  

## test your jar file
run jar file locally
or on the server
with java -jar jarfile.jar


## system process
cd /etc/systemd/system/
sudo touch financeapi.service

File contents:
```bash
[Unit]
Description=financeapi service
After=network.target

[Service]
Type=simple
User=gitlab-runner
WorkingDirectory=/home/gitlab-runner/api-deployment
ExecStart=/usr/bin/java -jar finance-api.jar 
Restart=on-abort

[Install]
WantedBy=multi-user.target
```


Configuration must be reloaded
sudo systemctl daemon-reload

Process must be enabled
sudo systemctl enable financeapi

Start the service
sudo service financeapi restart

To check backend use (or check your logfiles)
sudo service financeapi status

## External config for backend
Create custom.yaml file in home folder of gitlab runner
Update service ExecStart:  
ExecStart=/usr/bin/java -jar -Dspring.config.location=/home/gitlab-runner/custom.yaml finance-api.jar

## Allow gitlab runner to restart backend
as ubuntu user type sudo visudo  
Add to the end following line:
gitlab-runner ALL = NOPASSWD: /usr/sbin/service financeapi *

## Where is it running at
http://ip_address:port/api_or_other_base_url/rest_url
http://13.49.159.43:8080/api  
http://13.49.159.43:8080/api/finance  
http://13.49.159.43:8080/api/finance?symbol=GOOGL  

## Install nginx
sudo apt-get install nginx

## Configure nginx reverse proxy
To get rid of the port add following config to nginx
```bash
location /api/ {  
    proxy_pass   http://localhost:8080;  
}
```
## Final nginx config
```bash
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/html;

	index index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {
		try_files $uri $uri/ =404;
	}

        location /api/ {  
                proxy_pass   http://localhost:8080;  
        }  
}
```
## Domain
freenom.com & register domain  
AWS53 and create hosted zone + A record  
copied NS (nameservers) to freenom custom nameserver  

## https
maybe need this? sudo add-apt-repository ppa:certbot/certbot  
sudo apt install python3-certbot-nginx  
sudo certbot --nginx  

and complete the setup

The end!
