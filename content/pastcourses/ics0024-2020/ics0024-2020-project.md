+++
title = "ICS0024 Project"
description = ""
weight = 1
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)

## Introduction
Course is done in teams of 2-4 people. You **must** have a team. Solo teams are not allowed. There is much to gain by working together.  

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

To pass you need to complete 1 project which is graded in 3 parts:
1) Tested Modern Application 
1) Selenium and CypressJs
1) Testing Automation, CI/CD pipeline

### Registering a team
1) Register in excel
   * excel removed
1) Create a **private** channel with name team_number_teamname where number is your team number, 
teamname is your team name (team name is optional)
    1) For example: team_14_happyoranges
    1) Important: teams < 10 use 0 before your number, so team_03_funnyarchitects
        1) It helps with slack order (for us)
    1) Create channel in ics0024-2020 slack, no need for separate slack
1) Invite all your team members and all [mentors](../mentors) to that channel

### Topic
Topic is used for parts 1 and 3
1) Select an external API. In the class we are using Alpha Vantage, however you can choose any one of the suggested ones -> [api](../../guides/external-api)
1) Pick a service from external API. It should be an endpoint. 
    1. For example Alpha Vantage has time_series_daily: https://www.alphavantage.co/documentation/#daily
    1. It returns data what looks like this: https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=IBM&apikey=demo
1) Do some sort of calculation or manipulation with the data you've retrieved.
    1) Here are some of my suggestions [calculation options](https://docs.google.com/document/d/1OluPE9LCLqyWUJCQGtQb3eZPU7U84uFORHbFgR0eesA/edit?usp=sharing)
1) **NB!** Confirm your topic with me. It is allowed to have only one topic per team.
1) Topics should be selected by the end of L5

## Part 1 "Tested modern application"

### Goal
Build a modern backend API and test it with different tools.

### What are we doing
todo update
1) Each team is building an backend API calculator
    * backend API is something that takes in stock code/ticket, maybe some other parameters (like dates) and returns some result.
1) Calculator is structured as backend REST API
    * You can use class code as an example of API (delete everything unused)
    * You need 1 public GET or POST method
    * All logic should be done with 1 request
1) As this is testing course, most of the points are given for tests.
    * Another big areas are 1) calculator API and 2) defense 
    * For full disribution [grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing) 

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0024/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Submitting
* Team must have a team chat with all of the team members and [mentors](../mentors)
    * We use team chat for communication and help. Contact directly only on personal matters.
    * Using team chats enables us to identify and tackle common problems.
* Teams must use use taltech gitlab (gitlab.cs.ttu.ee)
    * Add lecturer and all mentors to your gitlab project **as reporter**
        * usernames are in [mentors](../mentors)
    * No access to repository the same as no submission
* Team must confirm with the lecturers that they can access the code
    * As there are many teams we are not able to connect email invitations with teams. 
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our project repository <link to repository>. We have given you reporter access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are so many and we have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)


### Grading
{{< readfile file="/fragments-2020/ics0024/grading-1.md" markdown="true" >}}

### Defence
{{< readfile file="/fragments-2020/defence.md" markdown="true" >}}
        
## Part 2 "Selenium and CypressJs"

### Goal
Learn how testers write UI tests.

### What are we doing
* Create 2 new repositories, 1 for java + selenium and 1 for js + cypressJs
    * +1 repo and language + framework if you are feeling adventurous
* Write tests for https://the-internet.herokuapp.com/ or any other public website.
    * There must be 8 tests in total, 4 tests for 2 business cases  
    * Please confirm _other public website_ 
* **NB!!** All of your repositories must have exactly the same tests
    * Our goal is to compare

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0024/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Submitting
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our selenium repository <link to repository>. We have given you reporter access. 
    Here is our cypressJs repository <link to repository>. We have given you reporter access.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are many teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
{{< readfile file="/fragments-2020/ics0024/grading-2.md" markdown="true" >}}

### Defence
* There is no defence

## Part 3 "Production pipeline"
Continue working with your team and with your code from part 1.

### Goal
1) Commit to main/master branch triggers gitlab CI/CD pipeline. Code is compiled, tested, deployed to the server, application is restarted.  

### What are we doing
* Your team needs an empty virtual server. Pick one: AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
    * for Etais send @OlegPahhomov your public ssh key
* You need to setup gitlab CI/CD setup for backend repository.
* You need to setup production
    * Backend is running on the server as a linux service
    * Backend is proxied by web server
    * Or use some combination of docker to replace one (or all) of the above (optional)
* You need to create a detailed guide about all of the above 
* For more details: [grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)

### A component diagram
Create a component diagram of your **actual** setup. Create your own picture as using mine is not allowed.  
<img src="../component_diagram.png">
Write a description.
{{< panel title="Component diagram" style="secondary" >}} 
Production is set up using AWS EC2. EC2 is an empty Ubuntu 20.04x virtual machine. Server was configured by hand.
For installation guide please follow to [link to installation guide].
Gitlab ci is used for build and deploy process. It is configured to test all branches and deploy the main branch. 
Gitlab runner and production are installed on the server.
For gitlab runner shell installation is used. Gitlab builds jar file for backend.
Backend is setup as linux process. Backend configuration is in external .yaml file. 
Backend is connected to frontend on nginx using reverse proxy. Our domain is from freenom. 
DNS is done by Route 53.
{{< /panel >}}

### Alternative technologies
{{< readfile file="/fragments-2020/ics0024/alternatives.md" markdown="true" >}}

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0024/deadline-project-3.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

_*This deadline is for people who would like to leave for winter holidays. Experience has shown grading is slow._

### Submitting
* For grading we will commit to backend and frontend repositories expecting front-end and back-end production to change 
    * For that to work update us from **reporter** to **maintainer** 
    * Please confirm that we can push
* We also need access to your production server to validate your configuration
    * [mentors](../mentors) has our ssh keys
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here is our back-end repository <link to repository>. We have given all mentors maintainer access. 
    Here is our wiki/pages <link to wiki/pages>. All mentors can access. For wiki we have home page in wiki (it fixes access problems).
    Here is our installation guide <link to guide>. All mentors can access. 
    Here is our component diagram & description <link to both>. All mentors can access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)


### Grading
{{< readfile file="/fragments-2020/ics0024/grading-3.md" markdown="true" >}}

### Defence
{{< readfile file="/fragments-2020/defence.md" markdown="true" >}}

### Tips
{{< readfile file="/fragments-2020/cicd-tips.md" markdown="true" >}}

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2 + p3) / (p1max + p2max + p3max) * 100  
```  

{{< table style="table-striped" >}}
| percentage      | grade        | description  |
| ------------- |:---------------:| -----|
| 91-100 | 5 | excellent |
| 81-90  | 4 | very good |
| 71-80  | 3 | good |
| 61-70  | 2 | satisfactory |
| 51-60  | 1 | sufficient |
{{< /table >}}


[The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)
