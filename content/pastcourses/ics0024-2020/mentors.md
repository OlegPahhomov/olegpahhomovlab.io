+++
title = "ICS0024 Mentors"
description = ""
weight = 100
+++

## Lecturer

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}


{{< table style="table-striped" >}}
| name          | gitlab user   | role  |
| ------------- |-------------| -----|
| Oleg Pahhomov | olpahh        | lecturer |
{{< /table >}}

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}}
removed
{{< /code >}}
