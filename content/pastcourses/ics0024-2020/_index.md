+++
title = "ICS0024 2020"
description = ""
weight = 201
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1TRrvi6i-8I_F7gBzWaKDcXuwndfgwKG16GFP_dkL5is/edit?usp=sharing)


## ICS0024 Automated Testing 2020 Course info

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}


### Goal
1) Automated Testing, write tests:
    1) as develop
    1) as tester 
1) Testing Automation, take back-end API to production and add CI/CD pipeline

### Introduction
Course is on slack: https://ics0024-2020.slack.com  **To sign up**: https://ics0024-2020.slack.com/join/signup  
Slack is where I answer your questions and communicate with you.  
Slack is used by businesses around the world (although lately Teams has been growing faster).  
I recommend you create a bookmark folder and put everything from our course there (this is how I roll).

Course is 16 weeks long: there is a 1.5h lesson each week. Theory and practise are taught together.  
It is advisable to bring your own laptop, however there are also computers supplied by university.  
Course is recorded (audio success rate ~90%).  

Course ends with a **grade**. Course is done in teams of 2-4 people.  
To pass you need to complete 1 [project](ics0024-2020-project) which is graded in 3 parts:
1) Tested Modern Application 
1) Selenium and CypressJs
1) Testing Automation, CI/CD pipeline

### Materials
* [Lecture slides](https://drive.google.com/drive/folders/1PkaXWY_klgXxBo_M606pySgFrWWr7QWz?usp=sharing)
    * ``https://drive.google.com/drive/folders/1PkaXWY_klgXxBo_M606pySgFrWWr7QWz?usp=sharing``
    * will be updated eventually
* [Videos](https://echo360.org.uk/section/04cc01ba-86c0-4e58-ba02-4537346e6df5/public)
    * ``https://echo360.org.uk/section/04cc01ba-86c0-4e58-ba02-4537346e6df5/public``
    * **NB!** After you open it the link changes and new link won't work

### Class code
* [ICS0024-2020 intro code](https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-intro-code)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-intro-code``
* [ICS0024-2020 backend api](https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-backend-api)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-backend-api``
* [Selenium](https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-selenium)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-selenium``
* [Cypress](https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-cypress)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2020-cypress``
   
### Refresh java
* [git-practice.zip](https://drive.google.com/drive/folders/14HWd9vLkC68mw597tbp1d-ck3kLCMsvV?usp=sharing)
    * ``https://drive.google.com/drive/folders/14HWd9vLkC68mw597tbp1d-ck3kLCMsvV?usp=sharing``
* (Optional) [Java Zero to Hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    * ``https://github.com/OlegPahhomov/java-zero-to-hero``
    * Solutions are on branches solutions-2019 or solutions-2020  
* (Optional) [Review Slides](https://drive.google.com/drive/folders/14HWd9vLkC68mw597tbp1d-ck3kLCMsvV?usp=sharing)
    * ``https://drive.google.com/drive/folders/14HWd9vLkC68mw597tbp1d-ck3kLCMsvV?usp=sharing``
    * Read through slides if Java seems complicated, it gives a brief overview
* (Optional) [Java basics - ICS0014-2020-java-introduction](https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction``
    * Clone this if Java seems complicated, it has some good examples and explanations 
    * Solutions are on a branch solutions-2020  

### Child pages
{{< childpages >}}
