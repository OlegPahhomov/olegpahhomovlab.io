+++
title = "ITI0302 2021"
description = ""
weight = 100
+++
{{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## Goals
1) Write modern application (with separate back-end and front-end). 
1) Take it to production and add CI/CD pipeline.
1) Secure it with spring security.

## Web Application Project (2021 Autumn)
### Slack
Course is on slack: https://iti0302-2021.slack.com  **To sign up**: https://join.slack.com/t/iti0302-2021/signup  
Slack is for questions, announcements and communication with the lecturer _(important announcements will be sent to email as well)_  
Slack is used by businesses around the world (although lately Teams has been growing faster).  

### Structure
Course is 16 weeks long: there is a 3h lesson each week. Theory and practise are taught together. Course is divided into 3 parts to match goals. 
All lectures will be recorded. 1st part will be focused for on-site, 2nd & 3rd parts are focused for on-line. 
It is advisable to bring your own laptop, however there will be computers supplied by university.  
Course is recorded (audio success rate ~90%).  
We will keep a broad overview of what we did in the class info [ITI0302 Class](iti0302-2021-class) 

### Mentor
Each team gets a [mentor](iti0302-2021-mentors). Mentor is there to jumpstart your programming journey. Mentor will be used most extensively for part 1.
Mentor stays with one team during whole course.

### Grade
Course ends with a **grade**. Course is done in teams of 2-4 people.
You will be working with the same team on the same [project](iti0302-2020-project) (application) for the duration of the course. Your application will be graded 3 times.
1) Simple Modern Application ~ 8 weeks
1) Gitlab CI/CD, production  ~ 4 weeks
1) Securing with Spring, finalizing application ~ 4 weeks


## Recordings, materials, code

### Recordings
* [Videos](https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public)
    * ``https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public``
    * **NB!** After you open it the link changes and the new link won't work
    * Recordings are LIVE only for on-site lectures (as an alternative I could record a video myself)  

### Slides
* [Lecture slides](https://drive.google.com/drive/folders/1r_hmQuAVOpXDnPaZBceESHRJKxEsqidY?usp=sharing)
  * ``https://drive.google.com/drive/folders/1r_hmQuAVOpXDnPaZBceESHRJKxEsqidY?usp=sharing``

### Class code
* [Java-Heroes back-end](https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-back)
    * ``https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-back``
* [Angular-Heroes front-end](https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-front)
    * ``https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-front``
* [React-Heroes front-end](https://gitlab.cs.ttu.ee/ansalt/iti0302-2021-heroes-front-react) by Andreas Saltsberg
    * ``https://gitlab.cs.ttu.ee/ansalt/iti0302-2021-heroes-front-react``

### Refresh git, java
* [git-game.zip](https://drive.google.com/file/d/1df1AG6iIFoMtaUZwiFGYxZCB85_mSe8s/view?usp=sharing)
  * ``https://drive.google.com/file/d/1df1AG6iIFoMtaUZwiFGYxZCB85_mSe8s/view?usp=sharing``
* [Java Zero to Hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    * ``https://github.com/OlegPahhomov/java-zero-to-hero``
    * Solutions are on branches solutions-2019 or solutions-2020  
* (Optional) [First time Java guides](/guides/first-time-java)
  * Read through easy guides if Java seems complicated, it gives a brief overview
* (Optional) [Review Java Slides](https://drive.google.com/drive/folders/1g0eB61Cibako_GqarlRV3_coNxrBU935?usp=sharing)
  * ``https://drive.google.com/drive/folders/1g0eB61Cibako_GqarlRV3_coNxrBU935?usp=sharing``
  * Read through slides if Java seems complicated, it gives a brief overview
* (Optional) [Java basics - ICS0014-2021-java](https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java``
    * Clone this if Java seems complicated, it has some good examples and explanations 
    * Solutions are on a branch solutions-xtz  

### Setup
For course part 1
1) [git](/guides/git)
1) [back-end](/guides/back-end)
1) [front-end](/guides/front-end)

For course part 2
1) [docker](/guides/docker)
1) [production](/guides/production)
1) class machine (will be added in part 2)
1) spring security (will be added in part 3)

[comment]: <> (1&#41; [class machine]&#40;iti0302-2020-production&#41;)

### Child pages
{{< childpages >}}
