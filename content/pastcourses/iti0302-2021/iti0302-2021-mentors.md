+++
title = "ITI0302 Mentors"
description = ""
weight = 400
+++
{{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## Mentors
The goal of the mentor will be to jumpstart your programming journey.

{{< table style="table-striped" >}}
| name          | gitlab   | role  | Mon* | !Mon* | Online* | angular | react | vue |  aws | azure | gc* | kotlin | 
| --------------------------------------------------------------------------- |------------  | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | 
| <a href="https://www.linkedin.com/in/olegpahhomov/">Oleg Pahhomov</a> | olpahh | lecturer | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | 
| <a href="https://www.linkedin.com/in/andreas-saltsberg-ab46b3138/">Andreas Saltsberg</a> | ansalt   | mentor | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| <a href="https://www.linkedin.com/in/glebkomissarov/">Gleb Komissarov</a>   | glkomi   | mentor | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| <a href="https://www.linkedin.com/in/ilja-samoilov-45752986/">Ilja Samoilov</a>    | ilsamo   | mentor | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| <a href="https://www.linkedin.com/in/kristjan-variksoo-83b579167/">Kristjan Variksoo</a> | kvarik   | mentor | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| <a href="https://www.linkedin.com/in/lilia-t%C3%BCnts-274959187/">Lilia Tünts</a>       | litunt   | mentor | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| <a href="https://www.linkedin.com/in/nele-sergejeva-30b708bb/">Nele Sergejeva</a>    | nele.sergejeva | mentor | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> |
| <a href="https://www.linkedin.com/in/tavo-annus-4a5631171/">Tavo Annus</a>        | taannu   | mentor | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> |
| <a href="https://www.linkedin.com/in/valeri-andrejev">Valeri Andrejev</a>    | vaandr      | mentor | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
{{< /table >}}

*Mon* - visits Monday team session with 50% frequency  
*!Mon* - visits school on some other day  
*Online* - is available online (prefers to be only online if Mon or !Mon are crossed)  
*gc* - google cloud  
*angular/react/vue* - front-end preference, will survive crosses 
*aws/azure/gc* - cloud provider preference, will survive crosses

**NB!!** On-site lectures happen only for part 1 (~ first 7 weeks). How you meet your mentors after part 1 is up to you.

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}} 
removed
{{< /code >}}
