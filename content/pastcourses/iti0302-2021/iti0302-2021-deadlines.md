+++
title = "ITI0302 Deadlines"
description = ""
weight = 2
+++

{{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## Project deadlines

### Part 1
[Simple modern application](../iti0302-2021-project/#part-1-simple-modern-application)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/iti0302/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 1 defences
Register with doodle sent in team chats. Info on [defences](../iti0302-2021-project/#defence)

### Part 2
[Gitlab CI/CD, production](../iti0302-2021-project/#part-2-gitlab-cicd-production)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/iti0302/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 2 defences
No defence

### Part 3
[Securing with Spring, finalizing application](../iti0302-2021-project/#project-part-3-securing-with-spring-finalizing-application)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/iti0302/deadline-project-3.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 3 defences
Register with doodle sent in team chats. Info on [defences](../iti0302-2021-project/#defence-2)

## Homework deadlines

### Team retro after project 1
This is your 2nd graded retro (1st is for Checkpoint 2). 

**NB!!** This has extra 4th question.
1. What worked well?
2. What went bad? / What could be improved?
3. What will we commit to improve in the next Sprint?
4. What's our progress compared to last retro?

You can use service like https://easyretro.io/

| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| deadline | Tuesday 19.10 23:59 | for project 1 early birds |
| deadline | Tuesday 26.10 23:59 | for project 1 on time |
| deadline | Tuesday 02.11 23:59 | for project 1 late |

[comment]: <> (### Retro 1)

[comment]: <> ([Team based retro]&#40;../iti0302-2021-class/#team-based-retro&#41;)

[comment]: <> ({{< table style="table-striped" >}})

[comment]: <> (| deadline      | date        | notes  |)

[comment]: <> (| ------------- |:-------------:| -----|)

[comment]: <> (| deadline | tbd |  |)

[comment]: <> ({{< /table >}})

[comment]: <> (_works after **deadline** are 0_)

[comment]: <> (### Retro 2)

[comment]: <> ([Team based retro, retro 2 vs retro 1]&#40;../iti0302-2021-class/#team-based-retro-retro-2-vs-retro-1&#41;  )

[comment]: <> (Do this retro **after** completing part 1  )

[comment]: <> ({{< table style="table-striped" >}})

[comment]: <> (| deadline      | date        | notes  |)

[comment]: <> (| ------------- |:-------------:| -----|)

[comment]: <> (| deadline 1 | tbd | this deadline is for part 1 early birds |)

[comment]: <> (| deadline 2 | tbd | this deadline is for everyone |)

[comment]: <> ({{< /table >}})

[comment]: <> (_works after **deadline** are 0_)

[comment]: <> (### Retro 3)

[comment]: <> ([Team based retro, retro 3 vs retro 2]&#40;../iti0302-2021-class/#team-based-retro-retro-3-vs-retro-2&#41;  )

[comment]: <> (Do this retro **after** completing part 2  )

[comment]: <> ({{< table style="table-striped" >}})

[comment]: <> (| deadline      | date        | notes  |)

[comment]: <> (| ------------- |:-------------:| -----|)

[comment]: <> (| deadline 1 | tbd | this deadline is for part 2 early birds |)

[comment]: <> (| deadline 2 | tbd | this deadline is for everyone |)

[comment]: <> ({{< /table >}})

[comment]: <> (_works after **deadline** are 0_)
