+++
title = "ITI0302 Spring Security"
description = ""
weight = 500
+++

## Spring Security

Spring Security is a powerful and highly customizable authentication and access-control framework. It is the de-facto standard for securing Spring-based applications.  

Let's unpack previous sentences:
1. authentication - who are you?
2. access-control - also known as authorization, what are you allowed to do?
3. de-facto standard - 99.9% of Java-Spring applications use it

{{< alert style="warning" >}} 
Spring Security will be harder to search and copy from the internet 
1) it requires multiple different classes
2) there are multiple different ways it can be done
{{< /alert >}}

I think the best way to start is 
1. to read parts of the official guide
2. to follow class videos

### official guide
From the guide linked below read following chapters:
1. (first 4 are in a row)
2. Spring Security Architecture (only the introduction part)
3. Authentication and Access Control (only the introduction part)
4. Authentication
5. Customizing Authentication Managers
6. (and then scroll lower for)
7. Method Security  

https://spring.io/guides/topicals/spring-security-architecture

## Class code
In class videos I will start with no security and move to basic and then to jwt. 
The further rabbit-hole I go the more classes I will generate (both in the front and back).

In the front-end we will add 
1. AuthGuard interceptor to protect urls
2. Login form & functionality
3. Registration form & functionality


For our project we will focus on 2 implementations:
1. **http-basic**
   1. sessions held and managed by backend application 
   2. not used for open-world apps, but easy
2. **jwt** with login & registration forms 
   1. sessions held by the frontend and managed in the database 
   2. more secure, but more complex

Our grading encourages you to use jwt with login & registration forms.


### http-basic

* Login is done by the browser
* Users are declared in memory
* No registration

Branches to use

|Back-end | Front-end  | 
|---|---|
|feature/part-3-http-basic|feature/part-3-http-basic*|

feature/part-3-http-basic* - for front-end there are no changes, the branch is created for symmetry 

### jwt

* Login is saved on the front-end 
* Users are saved to the database
* Registration (optional)

|Back-end | Front-end  | 
|---|---|
|feature/part-3-jwt|feature/part-3-jwt|

## Tutorials and Links

* Why and how of Spring Security
    * https://spring.io/guides/topicals/spring-security-architecture
    * https://dzone.com/articles/spring-security-authentication
    * https://spring.io/blog/2013/07/03/spring-security-java-config-preview-web-security/
* Http Basic Baeldung
    * https://www.baeldung.com/spring-security-basic-authentication
* Managing cookies
    * https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?utm_source=chrome-ntp-icon
* JWT: Json web token
    * https://jwt.io/
* Spring Boot + Angular JWT:
    * https://dzone.com/articles/angular-7-spring-boot-jwt-authentication-example
* JWT registration on Angular
    * https://jasonwatmore.com/post/2019/05/22/angular-7-tutorial-part-5-registration-form-user-service
