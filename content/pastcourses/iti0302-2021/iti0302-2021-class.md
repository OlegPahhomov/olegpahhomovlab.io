+++
title = "ITI0302 Class"
description = ""
weight = 400
+++
{{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## recordings for part 3
General guide on [spring security](../iti0302-2021-spring-security/)  
[Course videos](https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public)

0 video - introduction to the points {{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}  
0.1 video - introduction to general guide [spring security](../iti0302-2021-spring-security/)  
0.2 video - how to do part 3? (comments after finishing filming the course)  
1-8 are included in **feature/part-3-http-basic**  
1-36 are included in **feature/part-3-jwt**  

1. add spring security libraries, run tests  
   1.1 default password?  
```groovy
implementation group: 'org.springframework.boot', name: 'spring-boot-starter-security'
implementation group: 'org.springframework.security', name: 'spring-security-test'
annotationProcessor group: 'org.springframework.boot', name: 'spring-boot-configuration-processor'
```
   _annotationProcessor was added during point 7_  

2. default config doesn't suit us so change to basic
```java
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) {
        ...
    }
}
```
3. configure session to be stateless, disable csrf & disable https
   3.1 tests pass  
```java
@Override
protected void configure(HttpSecurity http) throws Exception {
   ...
}
```
4. configure in-memory users  
   4.1 1st user  
   4.2 2nd user (admin)  
   4.3 add ApplicationRoles class  
   4.4 make users configurable through yaml  
```java
@Autowired
public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
    ...
}
```
5. me endpoint
```java
@GetMapping("me")
public Object getMe() {
   ...
}
```
```java
public class UserUtil {
   ...
}
```

6. add permissions by url
```java
http.authorizeRequests()  
   .antMatchers("/user").hasRole("USER")
```
7. add permission by annotation (preferred)
```java
SomeController {
    @Secured(ApplicationRoles.USER)
    @GetMapping
    public Whatever method() {
        ...
    }
}
```
8. recap & read theory [theory](https://spring.io/guides/topicals/spring-security-architecture), [spring security page](../iti0302-2021-spring-security/)
9. jwt plan going forward [draw.io drawing](https://drive.google.com/file/d/1-lRiNFQT5CtA2BUJMS6rd6xJSdIKKhpG/view?usp=sharing)  
   **switch to branches feature/part-3-jwt**
10. what is jwt [jwt.io](https://jwt.io)
11. add jwt & jwt token provider  
    11.1 psvm generate token for trying it  
    11.2 springify JwtTokenProvider + external configuration  
    11.3 getUsernameFromToken, hasExpired, isValid
```groovy
compile group: 'io.jsonwebtoken', name: 'jjwt', version: '0.9.1'
```
```java
public class JwtTokenProvider {
   ...
}
```

12. spring security with jwt filter
```java
http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
```
```java
public class RestAuthenticationEntryPoint {
   ... 
}
```
13. jwt request filter contents
```java
public class JwtRequestFilter extends OncePerRequestFilter {
   ... 
}
```
14. try jwt with postman [postman](https://www.postman.com/)
15. do not apply spring security for swagger
```java
@Override
public void configure(WebSecurity web) throws Exception {
   web.ignoring()...;
}
```
16. users login endpoint  
   16.1 recap user login
```java
@PostMapping("login")
public LoginResponse login(@RequestBody LoginRequest loginRequest) {
   ... 
}
```
```java
public class UserService {
   ...
}
```
```java
@Bean
public AuthenticationManager authenticationManager() throws Exception {
   return super.authenticationManager();
}

@Bean
public UserDetailsService userDetailsService() {
   return super.userDetailsService();
}
```

17. frontend plan & prep with cleanup
```bash
rm *.spec
mkdir model
mkdir service
```
18. frontend login form with validation  
    18.1 formgroup fix & css  
[ng generate](https://angular.io/cli/generate),  
[angular-material form](https://material.angular.io/components/form-field/overview),  
[form validation](https://angular.io/guide/form-validation)
```bash
ng generate component login 
```
19. add user service and post LoginRequest to back-end
```bash
ng generate class LoginRequest
ng generate service user
```
20. What is LocalStorage?
```bash
localStorage.setItem
localStorage.getItem
localStorage.removeItem
```
21. add LoginResponse class  
    21.1 improve login response logging
```bash
ng generate class LoginResponse
```
22. add authenticationService & save token to localStorage  
    [BehaviorSubject vs Observable](https://stackoverflow.com/questions/39494058/behaviorsubject-vs-observable)
```bash
ng generate service authentication
```
23. add jwt interceptor to add token to requests
```bash
ng generate interceptor jwt
```
24. log user out on 401 (unauthorized) errors
```bash
ng generate interceptor error
```
25. add auth guard to police private urls
```bash
ng generate guard auth
```
26. redirect user on login
27. make dashboard page public
28. add admin role from spring authority
29. hide login page from logged-in user
30. make only admin capable of deleting heroes
31. plan for registering users [draw.io drawing](https://drive.google.com/file/d/1-lRiNFQT5CtA2BUJMS6rd6xJSdIKKhpG/view?usp=sharing) 
&& registration form
```bash
ng generate component register
ng generate class RegisterRequest
```
32. registration endpoint
```java
@PostMapping("register")
public ResponseEntity<Void> register(@RequestBody RegisterRequest registerRequest) {
   ...
}
```
33. create users table with 2 users
```java
@Entity
public class User {
   ...
}
```
34. **NB!!** user login bugfix
    34.1 solve undefined currentUser
    34.2 protect against future undefined currentUser
35. make spring use our database users in checking users 
```java
@Service
public class MyUserDetailsService implements UserDetailsService {
   ...
}
```
```java
@Autowired
public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
   ...
}
```
36. define MyUser to improve on UserDetails
```java
public class MyUser extends User {
   ...
}
```
37. [bonus] add lombok, branch feature/part3-jwt-lombok
38. summary

Good luck!
* [Videos](https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public)
   * ``https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public``
   * **NB!** After you open it the link changes and the new link won't work

## recordings for part 2

[prod guides](/guides/production/)   
1. Backend .jar file. How to package and run Java applications?
2. Frontend dist folder. How to package and run frontend applications?
3. What is a server? What is cloud?
4. Getting server (aws, azure, digitalocean or other) [prod guides](/guides/production/)  
    goodies: https://education.github.com/pack  
    aws is not part of googies anymore (aws educate does not give credits)  
   4.1 ssh keys    
5. how to get code up?  
6. gitlab runner for backend [prod guides](/guides/production/)  
6.1 install java to pass gradle build  
6.2 fix ci not running deploy 
7. gitlab runner for frontend [prod guides](/guides/production/)    
7.1 scared of angular memory  
7.2 gitlab runner for frontend 2 + node  
8. linux service
9. linux service restart
10. nginx w random html
11. nginx w frontend  
11.1 nginx config for frontend and backend  
12. domain w aws routing
13. https
14. Docker and docker-compose [docker guides](/guides/docker/dockerfile/)  
15. Summary {{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

Good luck!

* [Videos](https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public)
   * ``https://echo360.org.uk/section/8a90516c-8272-4a16-8d69-7b4fcc9006cb/public``
   * **NB!** After you open it the link changes and the new link won't work

## 7th lesson 2021-10-11
1. No official lesson.
2. Work on your projects.
3. If you need help lecturer and mentors are on slack.

Lesson 8 & beyond course is **online**:
1. I will pre-record videos
2. Big topics
   1. Project part 2: CI/CD for production
   2. Project part 3: spring security

## 6th lesson 2021-10-04
1. Go through project 1/3 points
2. Test coverage
3. Add angular-material & how to use browser
   1. Add header for navigation
   2. How to analyse styles in the browser
   3. Use angular-materials toolbar and buttons
   4. Add flex so menu changes when in mobile
   5. List of heroes
   6. Add hero button
4. Show hero equipment on the front-end
5. Remove all unused code from front-end, so you know that old code should be deleted
6. Demo some External API (because it's fun?)
7. Finish back-end tests
8. Anything else class wanted?


## 5th lesson 2021-09-27
1. Checkpoint 2 introduction and questions
2. Recap last lesson - database!?
3. Back-end testing with MockMvc
   1. Example Controller tests 4.x
   2. Heroes Controller tests 5.x 
4. Add another database entity on the back-end
   1. back-end todo section 6.x

**NB!!** 2th October 23:59 [Checkpoint 2 deadline](/iti0302-2021/iti0302-2021-deadlines/)  
Details in {{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## 4th lesson 2021-09-20

1. Finish [back-end](https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-back) todo section 2
2. Database
   1. Slides 10
3. Add database to back-end
   1. Class code todo 3.x
4. Back-end project structure, division of objects
   1. Slides 8

## 3rd lesson 2021-09-13

1. Introduction to front-end applications
   1. Slides 9
2. Setup git to track class code
   1. [angular guide](/guides/front-end/angular/)
   1. [class-git](/guides/git/class-git/)
   2. Repo to use in the guide: https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-front
4. Build your first application using https://angular.io/tutorial
   2. [continue form here](https://angular.io/tutorial/toh-pt0#serve-the-application)
   3. We will be doing this as part of class code
5. Connect front-end with back-end

**NB!!** 18th September 23:59 [Checkpoint 1 deadline](/iti0302-2021/iti0302-2021-deadlines/)  
Details in {{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

## 2nd lesson 2021-09-06

1. Introduction to back-end applications
   1. Slides 6.0, 6.1, 6.2
2. Setup git to track class code
   1. [class-git](/guides/git/class-git/)
   2. Repo to use in the guide: https://gitlab.cs.ttu.ee/olpahh/iti0302-2021-heroes-back
3. Class code and follow along
   1. todo 1.x Experiment w Controllers endpoints
   1. todo 2.x API endpoints for Heroes webpage



## 1st lesson 2021-08-30

1. Introduction for the course and team
   1. Slides 0, 1
2. Install everything for backend:
    - [back-end](/guides/back-end)
    - _once done move to next step_
3. Git game
    - Gitlandia needs your help. Create an epic hero and put an end to monster invasion!
    - Groups of **2-4** people
    - [Course home -> refresh-git-java](../#refresh-git-java) -> git-practice.zip
      - [direct link](https://drive.google.com/file/d/1df1AG6iIFoMtaUZwiFGYxZCB85_mSe8s/view?usp=sharing)
    - Open it with IntelliJ
    - Follow README.md
    - Later follow ReadyPlayerXyz.java
        - _there are no right or wrong answers, the code should compile and run_
    - Ultimately this exercise is about getting everything to work
    - _once done move to next step_    
4. Java zero to hero
    - Individual work
    - Clone [https://github.com/OlegPahhomov/java-zero-to-hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    - There are todos scattered all over the code, levels 0 to 15
      - Work on assignments that interest you  
      - _there are solutions on branches solutions-2019 and solutions-2020_ 
    - _once done move to next step_
5. (Optional) You can review slides about Java
   - [Course home -> refresh-git-java](../#refresh-git-java)
   - or how about some basic guides [first-time-java](/guides/first-time-java)
6. Project part 1 starts next week
    - Get a team (and topic?)
    - Review [project guide](../iti0302-2021-project) and review {{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}


