+++
title = "ITI0302 Class Machine"
description = ""
weight = 401
+++
{{< readfile file="/fragments-2021/iti0302/grading-excel.md" markdown="true" >}}

{{< alert style="warning" >}} This guide is intentionally minimalistic. For it is your goal to write a proper guide yourself. {{< /alert >}}

## Initial setup 
I am using ubuntu 20.04. All commands are done on the server unless specified differently.

## connecting to the server
ssh ubuntu@13.53.39.145

## virtual memory
Aka we can survive angular build

Running htop will show how much memory you have.
https://itsfoss.com/create-swap-file-linux/  
For 2Gb virtual memory
```bash
sudo fallocate -l 2G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  
sudo swapon -show  
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
```

htop to check restart server and check whether virtual memory change was permanent sudo reboot

## gitlab runner
use class guide  

## install java
sudo apt-get update  
sudo apt-get upgrade  
sudo apt-get install openjdk-11-jre openjdk-11-jdk  

## test your jar file (optional)
run jar file locally
or on the server
with java -jar jarfile.jar
http://ip_address:port/api_or_other_base_url/rest_url
http://13.53.39.145:8080/api/

## Frontend
Install node on linux
sudo curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

## Backend service
cd /etc/systemd/system/
sudo touch heroes.service

File contents:
```bash
[Unit]
Description=dashboard heroes service
After=network.target

[Service]
Type=simple
User=gitlab-runner
WorkingDirectory=/home/gitlab-runner/api-deployment
ExecStart=/usr/bin/java -jar heroes-backend.jar
Restart=on-abort

[Install]
WantedBy=multi-user.target
```

Configuration must be reloaded
sudo systemctl daemon-reload

Process must be enabled
sudo systemctl enable heroes

Start the service
sudo service heroes restart

To check backend use (or check your logfiles)
sudo service heroes status

## Allow gitlab runner to restart backend
as ubuntu user type sudo visudo  
Add to the end following line:
gitlab-runner ALL = NOPASSWD: /usr/sbin/service heroes *

## nginx
sudo apt-get install nginx
http://13.53.39.145/
go to /etc/nginx/sites-available and modify existing file by removing all comments

## Show your frontend
create a symlink from gitlab-runner front-end code to /var/www
sudo ln -s /home/gitlab-runner/front-deployment/ /var/www/front-deployment

changed location of the root: root /var/www/front-deployment;  
after changing nginx config restart it with sudo service nginx restart  
http://13.53.39.145/ shows our front

## Frontend urls break
Edit heroes file, add a rule for nginx to forward to index.html if it doesn’t have other files
Replace old location /

```bash
    location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }
```

## Backend proxy
Add to nginx file
```bash
    location /api/ {  
        proxy_pass   http://localhost:8080;  
    }  
```
Both http://13.53.39.145:8080/api/ and http://13.53.39.145/api/ work for backend

## final nginx file

```bash
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/front-deployment;

	server_name _;

        location /api/ {
             proxy_pass   http://localhost:8080;
        }

	location / {
             index index.html index.htm;
             if (!-e $request_filename){
                 rewrite ^(.*)$ /index.html break;
             }
        }

}
```

## domain
freenom.com & register domain
AWS Route 53 and create hosted zone + A record with ip of our server
copied NS (nameservers) to freenom custom nameserver

## https
sudo apt-get install python3-certbot-nginx  
sudo certbot --nginx

and complete the setup

The end!
