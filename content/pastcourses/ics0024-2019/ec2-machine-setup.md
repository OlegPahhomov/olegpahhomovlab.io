+++
title = "ICS0024 (2019) machine setup"
description = ""
weight = 100
+++
{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

This is the guide for ICS0024.  
It contains only back-end setup.  
Full server setup (back-end, front-end) is available for [iti0203](../iti0203-2019/class-machine-setup.md)

Configuration for AWS server  
Ubuntu 18.04  
IP: 13.48.59.179  
All of the commands are done on the server.  

**server: login**   
First connect with IP the amazon is giving  
ssh: ssh ubuntu@13.48.59.179

**server: add ssh keys**  
cd ~/.ssh for every user (ubuntu is enough)  
nano authorized_keys, add the key  
sudo service ssh restart

**server: virtual memory**
Use when machine has low memory (1Gb)  

https://itsfoss.com/create-swap-file-linux/  

For 2Gb virtual memory  
sudo fallocate -l 2G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  
sudo swapon --show  
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab  

htop to check
restart server and check whether virtual memory change was permanent
sudo reboot

**backend: install open-jdk for Gitlab to build code and open-jre to run .jar**  
sudo apt-get update  
sudo apt-get install openjdk-11-jre openjdk-11-jdk  

**backend: install gitlab-runner on your server**  
Follow one of the class guides and add gitlab-ci.yml to your project  

**backend: define backend as linux service**    
*We assume that gitlab puts backend jar into /home/gitlab-runner/api-deployment*  
*We assume that jar is called heroes.jar*

cd /etc/systemd/system/  
sudo touch heroes.service

//start-copy  
[Unit]  
Description=dashboard heroes service  
After=network.target  

[Service]  
Type=simple  
User=gitlab-runner  
WorkingDirectory=/home/gitlab-runner/api-deployment    
ExecStart=/usr/bin/java -jar java-heroes.jar  
Restart=on-abort  

[Install]  
WantedBy=multi-user.target  
//stop-copy

Configuration must be reloaded    
sudo systemctl daemon-reload

Process must be enabled    
sudo systemctl enable heroes

Start the service  
sudo service heroes restart  

To check backend use (or check your logfiles)      
sudo service heroes status  
tail -f -n 500 location/filename  
-f is follow, -n is number of lines

**backend: where is it running**  
http://ip_address:port/api_or_other_base_url/rest_url  
http://13.48.59.179:8043/heroes  
Some servers (TalTech) don't expose random ports, only 80 and 443. So it will not open anything.

**backend: restart backend service after build W10**  
sudo service heroes restart  
Allow gitlab-runner to use sudo when restarting heroes service  
sudo visudo (add to the end)  
gitlab-runner ALL = NOPASSWD: /usr/sbin/service heroes *  
