+++
title = "ICS0024 2019 Project"
description = ""
weight = 1
+++

## ICS0024 2019 Project

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

### Introduction

This course we are building a modern backend REST API application.
Our application has a very high test coverage.
Application is built in teams 1-4 people.  

It is built and defended in 2 stages:
1) Tested modern application (automated testing)
1) Production pipeline (testing automation)


### Registering a team
1) Register in excel
    1) excel removed
1) Create a PRIVATE channel with name team_number_teamname where number is your team number, teamname is your team name (team name is optional)
    1) For example: team_14_souroranges
1) Invite all your team members to that channel
    * + @Oleg Pahhomov 

### Project part 1 "Tested modern application"

##### Registering
1) Pick a topic [class topics](https://docs.google.com/document/d/1UcfwMBon4PvcXrAH-85EmOvBVdd4XHBPwV1AaA9K_i8/edit?usp=sharing)
1) Contact me in team chat what you've chosen. 
    1) One topic per team. 
    1) In the following lectures I want to combine all your calculators to our class project, add production, pipeline and front-end.
1) If there are not enough topics, I will create more. 

##### What are we doing?
1) Each team is building a calculator
    * Calculator is something that takes in numbers, does some calculation and returns some result.
1) Calculator is structured as backend REST API
    * You can use class code as an example of API (delete everything unused)
    * You need 1 public method GET or POST /calculate
    * All of your calculation should be done with 1 request
1) As this is testing course, most of the points are given for tests.
    * Another big areas are 1) calculator API and 2) defense 
    * For full disribution [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=1935766078) 
1) The future - after project is done, we will add all calculators to our class project. To simplify the process here are some things to think about:
    * All calculators take in a list of numbers.
    * Some calculators return no result, some return a single result and others return many results. 
    * User could choose which calculation they want

##### Deadline and qualifying:
* Add as to your gitlab project **as reporter**:
    * For gitlab.cs.ttu.ee add @olpahh
    * For gitlab.com add @OlegPahhomov  
* **Working part1 of the project must be submitted on**
    * **Sunday (Oct 27th) morning 06.00**
        * **Saturday (Oct 26th) morning 06.00 (+x points)** 
    * **Sunday (Nov 3rd) morning 06.00** ***1 week late -30%***
* You can still keep working on you code, but I will grade everything up to deadline of your choice.
* If you miss these dates (miss your production deadline) you get 0.
    * Not accessing repositories is treated the same as missing the dates
        * I recommend you confirm with lecturer they can access your repositories.
        
##### Grading:
* Please follow the official table on how do you get graded. [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=1935766078)
Small tweaks might happen on W6, W7, W8.
* I will grade tag 1.0.0 or master branch
* You will all get personal feedback
* Small & beautiful is better than big and messy

##### Defence:
* Defences happening on:
    * **W9(Oct 28th) (last 1h of the class + as long as it takes)**
    * **W10(Nov 4th) (after class)**
* All team members must attend
* There will be a doodle
    * Doodle is given after submission of the work (first ones get to pick the best times)

### Project part 1.5 "Front-end" (optional)
Create a front-end for "combined calculators" project and get up to 300 extra points.
I will use your code in the class to show some front-end.

##### Deadline
* **24th November 06.00**

##### Description
Create a simple front-end for your application.
* Dropdown to pick calculator type
* Input field for entering values
* Button for OK
* Some result area where you show response as json

You must use angular, react or vue.  
Ideally use some UI component library like angular-material or bulma


Calculator types are available from
```bash
GET /api/options
http://13.48.59.179:8043/api/options
```
Calculator request is done by 
```bash
GET /api/calculate?calculatorType=ALL&values=1,2,3
http://13.48.59.179:8043/api/calculate?calculatorType=ALL&values=1,2,3
```
Calculator response is
```json
[
  {
    "type": "TEAM_2",
    "result": [
      1, 2, 3
    ]
  },
  {
    "type": "TEAM_6",
    "result": [
      1, 2, 3
    ]
  },
  ...
]
```
Swagger is available at
http://13.48.59.179:8043/api/swagger-ui.html#/calculator-controller  
**If links don't work let me know ASAP**

### Project part 2 "Production pipeline & Selenide"
Continue working with your team and with your code.

##### Goal
1) Commit to master branch (or new tag) triggers gitlab CI/CD pipeline. Code is compiled, tested, deployed to the server, application is restarted.  
2) Selenide tests on the website of your choice

##### What are we doing - production pipeline
* Your team needs an empty virtual server. Pick one: Etais (TalTech machines), AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
    * for Etais send @OlegPahhomov your public ssh key
* You need to setup gitlab CI/CD setup for backend and frontend (optional) repositories.
* You need to setup production
    * Backend is running on the server as a linux service
    * Frontend application served by web server (Nginx/Apache) (optional)
    * Backend is proxied by web server (optional)
    * Or use some combination of docker to replace one (or all) of the above (optional)
* You need to create a detailed guide about all of the above 
* For more details: [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=790428996)
* Front-end is optional. You can take class front-end code and use it 1:1. 
For smooth transition you will have to modify your API to match the one we have in the class

##### What are we doing - selenide tests
* You can use same or create new repository
    * Make sure selenide doesn't break your production pipeline 
* Pick any public website and create 2 x 10 step test there.

##### What are we doing - code improvements
* Improve your code or tests from part 1

##### How do we grade - production pipeline
* We will commit to backend and frontend repositories and expect front-end and back-end production to change 
* For that to work update us from **reporter** to **maintainer** 
    * For gitlab.cs.ttu.ee it's @olpahh
    * For gitlab.com it's @OlegPahhomov
* Points [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=790428996)

##### How do we grade - selenide
* Run tests from IntelliJ
* Points [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=790428996)

##### How do we grade - code improvements
* Add screenshots (or some other way) of before and after
* Points [grading excel](https://docs.google.com/spreadsheets/d/1D5s39gZMiZF64tB9zupG5MxGlp-a193PWVUsyq_gDAA/edit#gid=790428996)

##### Deadline reasons:
* Grade must be given before 22th of January. 
* Lecturers must provide at least 3 defense times
    * December 16th (last lecture)
    * January 6th, 13th
* We must have at least 24h to grade
* You can submit + defend before. Defense will happen during the last lecture. 16th December

##### Deadline
Read deadline reasons
* **Sunday, December 15th, morning 06.00**
    * **Friday, December 13rd, morning 06.00 (+xx points)**
    * **Saturday, December 14th, morning 06.00 (+x points)**
* **Sunday, January 5th, morning 06.00 (no penalty)**
    * **Friday, January 3rd, morning 06.00 (+xx points)**
    * **Saturday, January 4th, morning 06.00 (+x points)**
* **Sunday, January 12th, morning 06.00 1 week late** ***-30%***
* All works after the last deadline get -100% penalty
    * In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
* If you need an extension and have a valid reason(s), contact us.
    * Extension is not given on the last day.
    * Max extension is 1 week, as we just don't have more time

##### Submitting
* Team must provide production IP.
    * Team can grant ssh access to lecturers [ssh keys](ssh-keys)
* Team must give correct access for lecturers
    * Upgrade our rights to **maintainer** (so we can commit to master):
        * For gitlab.cs.ttu.ee it's @olpahh 
        * For gitlab.com it's @OlegPahhomov 
* Team must confirm with the lecturers that they can commit to master or push tags
    * As there are 30 teams we are not able to connect email invitations and teams. 
* **On the day of submission**, team must notify in team chat "@channel Done" so the lecturers know you are finished
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?

##### Defence:
* **Defences** for part 3 are happening on:
    * **W16(Dec 16th)** (1h either in the beginning or end of the class) 
    * **Jan 6th**
    * **Jan 13th**
* If none of the above work, let us know (in advance)
* All team members must attend the defence
* On Sunday morning after the project deadline a doodle will be sent out to teams that have submitted
* Defence lasts 15 min
    * Have your backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1 min presentation of your application. (Not the code)
    * 5 min for questions from lecturer (theory)
    * 5 min for questions to lecturer (including grades)  
    * Rest 4 minutes are for buffers, for small talk, for walking in and out etc.

### Final grade

Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.  
(v1 + v2) / (v1max + v2max) * 100  
91-100: grade 5 (excellent);  
81-90: grade 4 (very good);  
71-80: grade 3 (good);  
61-70: grade 2 (satisfactory);  
51-60: grade 1 (sufficient(.)  
