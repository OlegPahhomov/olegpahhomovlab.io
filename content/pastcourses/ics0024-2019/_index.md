+++
title = "ICS0024 2019"
description = ""
weight = 202
+++

## ICS0024 Automated Testing 2019 Course info

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

### Goal
Testing as develop, testing as a tester.
1) Write tests
1) Create automated CI/CD pipeline

### Introduction
Course runs during 2019 Autumn semester.

### Materials
* [Lecture slides](https://drive.google.com/open?id=1n0I1k1Afcax3-KYb4HcFN2yO6VsUgDpA)
* [Recordings](https://echo360.org.uk/section/0a48cf81-7e55-4892-829f-b6b65024b235/public)

### Class code
* [ICS0024-2019 intro code](https://gitlab.cs.ttu.ee/olpahh/ics0024-2019-intro-code)
* [Selenium automation](https://gitlab.cs.ttu.ee/olpahh/ui-automation-with-selenium)
* [Java Zero to Hero practise code](https://github.com/OlegPahhomov/java-zero-to-hero) 
   * for practising java, not used in the class

### Setup
1) [Backend](/guides/back-end)
1) [AWS](/guides/production)
1) [Machine setup guide](ec2-machine-setup.md)

### Project
[More info on project](ics0024-2019-project.md)  
