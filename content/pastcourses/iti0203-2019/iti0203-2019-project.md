+++
title = "ITI0203 2019 Project"
description = ""
weight = 2
+++

### Introduction

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

This course we are building a modern application.  
Application is built in teams 1-4 people.  

It is built and defended in 3 stages:
1) Simple, yet modern application
1) Production pipeline
1) Deep into Spring and Java


### Registering a team
1) Register in excel 
    1) excel removed
1) Create a PRIVATE channel with name team_number_teamname where number is your team number, teamname is your team name (team name is optional)
    1) For example: team_14_happyoranges
1) Invite all your team members to that channel +
    1) @Oleg Pahhomov 
    1) @Ilja Samoilov

### Topic

Goal is to build a modern back-end REST API and front-end that uses it. 
80% of points come from back-end.

Pick your own topic (recommended) or use on of mine. [lecturer topics](https://docs.google.com/document/d/1gRZVxAJq5MU_cqml38r1PgQtmgRdRmoDZfQF0bgCJMk/edit?usp=sharing)

We started building API from week 3 onwards.


### Project part 1 "Simple, yet modern application"


##### Analysis sheet:
Analysis sheet has multiple purposes. It is a tool for you and for us.
* It defines what you want to do
* It defines order of priorities.
* It is a tool for tracking where you are.
* It is a tool for dividing up responsibilities.
* It helps us grade. We grade you against what you planned to do. It is better to have something small and simple that works, then something big and complicated that doesn't.
* Analysis sheet will grow for part 2 and 3 of the project.
* You can ask feedback from us on your analysis sheet. We can tell you to be more specific, to have less stuff... etc, etc.  
* I recommend you use user stories + 1 user story for setting everything up.  
    * [User stories description](https://www.mountaingoatsoftware.com/agile/user-stories)  
    * [Class project user stories](https://docs.google.com/document/d/1sgqeR2vZ10mQ_UJe4qtD-tNOuQbvmCIaOXIzjB4n724/edit?usp=sharing)

##### Deadlines:
* **Working part1 of the project must be submitted on**:
    * **Sunday (W7 Oct 20th) morning 06.00**
        * **Saturday (Oct 19th) morning 06.00 (+x points)**
    * **Sunday (W8 Oct 27th) morning 06.00** ***1 week late -30%***
    * All works after the last deadline have -100% penalty
        * In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
    * If you need an extension and have a valid reason(s), contact us. 
        * Extension is not given on the last day.

##### Submitting
* Team must have a team chat with all of the members + Oleg and Ilja
    * We use team chat for communication and help. Contact directly only on personal matters.
* Team must share repositories in the team chat
    * Shared repositories will be added to grading excel
* Team must give correct access for lecturers
    * Add as to your gitlab project **as reporter** or above:
        * For gitlab.cs.ttu.ee it's @olpahh and @ilsamo
        * For gitlab.com it's @OlegPahhomov and @IljaSamoilov
    * No access to repository or analysis sheet is treated the same as no submission
* Team must confirm with the lecturers that they can access the code and analysis sheet
    * As there are 30 teams we are not able to connect email invitations with teams. 
* **On the day of submission**, team must notify in team chat "@channel Done" so the lecturers know you are finished
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?

##### Grading:
* Please follow the official table for grading points. [grading excel](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit?usp=sharing) Small tweaks might happen on W6 and W7.
* We will grade tag 1.0.x or master branch
* All of you will get personal feedback
* Small & beautiful is better than big and messy
* Spring Security setups are not graded (part 3 material)
//todo rewrite this part in the future

##### Defence:
* **Defences** for part 1 are happening on:
    * **W8(Oct 21th)** (2h either in the beginning or end of the class) 
    * **W9(Oct 28th)** (1h either in the beginning or end of the class)
    * If you are not able to participate on these 2 dates contact lecturers.
        * Part 1 must be defenced before deadline of part 2
* All team members must attend the defence
* On Sunday morning after the project deadline a doodle will be sent out to teams that have submitted
* Defence lasts 15 min
    * Have your backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1 min presentation of your application. (Not the code)
    * 5 min for questions from lecturer (theory)
    * 5 min for questions to lecturer (including grades)  
    * Rest 4 minutes are for buffers, for small talk, for walking in and out etc.

### Project part 2 "Production pipeline"

Continue working with your team and with your code.  

##### Goal
Commit to master branch (or new tag) triggers gitlab CI/CD pipeline. Code is compiled, tested, deployed to the server, application is restarted.  

##### What are we doing
* Your team needs an empty virtual server. Pick one: Etais (TalTech machines), AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
    * for Etais send @OlegPahhomov your public ssh key
* You need to setup gitlab CI/CD setup for backend and frontend repositories.
* You need to setup production
    * Backend is running on the server as a linux service
    * Frontend application served by web server (Nginx/Apache)
    * Backend is proxied by web server
    * Or use some combination of docker to replace one (or all) of the above
* You need to create a detailed guide about all of the above 
* For more details: [grading excel part 2](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit#gid=1231401474)
    * There are tons of extras!

##### How do we grade
* We will commit to backend and frontend repositories and expect front-end and back-end production to change 
* For that to work update us from **reporter** to **maintainer** 
    * gitlab.cs.ttu.ee @olpahh and @ilsamo
    * gitlab.com @OlegPahhomov and @IljaSamoilov
* Points [grading excel part 2](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit#gid=1231401474)

##### Deadlines
* **Working part2 of the project must be submitted on**:
    * **Sunday, Nov 17th (end of W11), morning 06.00**
        * **Friday, Nov 15th, morning 06.00 (+xx points)**
        * **Saturday, Nov 16th, morning 06.00 (+x points)**
    * **Sunday, Nov 24th (end of W12), morning 06.00** ***1 week late -30%***
    * All works after the last deadline get -100% penalty
        * In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
    * If you need an extension and have a valid reason(s), contact us. 
        * Extension is not given on the last day.

##### Submitting
* Team must provide production IP.
    * Team must grant ssh access to lecturers [ssh keys](ssh-keys)
* Team must give correct access for lecturers
    * Upgrade our rights to **maintainer** (so we can commit to master):
        * For gitlab.cs.ttu.ee it's @olpahh and @ilsamo
        * For gitlab.com it's @OlegPahhomov and @IljaSamoilov
* Team must confirm with the lecturers that they can commit to master or push tags
    * As there are 30 teams we are not able to connect email invitations and teams. 
* **On the day of submission**, team must notify in team chat "@channel Done" so the lecturers know you are finished
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?

##### Defence:
* There is no defence
    * We think committing to your repository and seeing what happens is enough

##### How to get help?
* Ask for help
    * Ask questions in team chat or general/project
    * Come to the lessons
* Server tips 
    * Keep your installation guide up to date
    * Add our ssh keys to your server so we can help you [ssh keys](ssh-keys)
* Part 2 is not that bad. Most of the must have section is done during the lecture of W8 (Oct 28th).
* **Start early**

### Project part 3 "Deep into Spring and Java"

Continue working with your team and with your code

##### Goal
Finalize. Application is finished. Spring Security for registration and roles (admin/user). Improved design. No bugs.

##### What are we doing
* We are working on our Application again.
* We are adding Spring Security for registration and roles (admin/user).
* We are fixing our bugs so application works as intended.
* We are adding angular-material so application looks nice.
* We are adding some daemon processes like synchronization because we can.

##### Deadline reasons:
* Grade must be given before 22th of January. 
* Lecturers must provide at least 3 defense times
    * December 16th (last lecture)
    * January 6th, 13th
* We must have at least 24h to grade
* You can submit + defend before. Defense will happen during the last lecture. 16th December

##### Deadline
Read deadline reasons
* **Sunday, December 15th, morning 06.00**
    * **Friday, December 13rd, morning 06.00 (+xx points)**
    * **Saturday, December 14th, morning 06.00 (+x points)**
* **Saturday, January 4th, morning 06.00 (no penalty)**
    * **Thursday, January 2nd, morning 06.00 (+xx points)**
    * **Friday, January 3rd, morning 06.00 (+x points)**
* **Saturday, January 11th, morning 06.00 1 week late** ***-30%***
* All works after the last deadline get -100% penalty
    * In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
* If you need an extension and have a valid reason(s), contact us.
    * Extension is not given on the last day.
    * Max extension is 1 week, as we just don't have more time

##### Submitting
* **On the day of submission**, team must notify in team chat "@channel Done" so the lecturers know you are finished
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?

##### Grading:
* Structure and points are similar to Part 1. Following things are new.
    * We want you to rewrite analysis sheet to show what was done in part 1 and what will be done in part 3. You can include refactoring and bugs in the planned tasks.
    * User stories are graded for working well. Bug-free. As expected.
    * Spring Security is graded. User can register. Admin/user can access their respective resources.
    * Part 2 still works.
    * Your application looks good in mobile.
* For more detailed descriptions look at the points. [grading excel](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit#gid=1938886936) 

##### Defence:
* **Defences** for part 3 are happening on:
    * **W16(Dec 16th)** (1h either in the beginning or end of the class) 
    * **Jan 6th**
    * **Jan 13th**
* If none of the above work, let us know (in advance)
* All team members must attend the defence
* On Sunday morning after the project deadline a doodle will be sent out to teams that have submitted
* Defence lasts 15 min
    * Have your backend and frontend code running. 
        * Projector has HDMI and VGA cables. Bring necessary dongles.
    * There is 1 min presentation of your application. (Not the code)
    * 5 min for questions from lecturer (theory)
    * 5 min for questions to lecturer (including grades)  
    * Rest 4 minutes are for buffers, for small talk, for walking in and out etc.

##### How to get help?
* Ask for help
    * Ask questions in team chat or general/project
    * Come to the lessons
* The end is near
* **Start early**

### Final grade

Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.  
(v1 + v2 + v3) / (v1max + v2max + v3max) * 100  
91-100: grade 5 (excellent);  
81-90: grade 4 (very good);  
71-80: grade 3 (good);  
61-70: grade 2 (satisfactory);  
51-60: grade 1 (sufficient).  
