+++
title = "ITI0203 2019 class machine setup"
description = ""
weight = 5
+++

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

Configuration for Taltech server (tested on AWS as well)  
Ubuntu 18.04  
IP: 193.40.155.144  
All of the commands are done on the server.  

**server: login W8/W9**  
ssh: ssh ubuntu@193.40.155.144

**server: add ssh keys W9**  
cd ~/.ssh for every user (ubuntu is enough)  
nano authorized_keys, add the key  
sudo service ssh restart

**server: virtual memory W8**
Use when machine has low memory (1Gb)  

https://itsfoss.com/create-swap-file-linux/  

For 2Gb virtual memory  
sudo fallocate -l 2G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  
sudo swapon --show  
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab  

htop to check
restart server and check whether virtual memory change was permanent
sudo reboot

**backend: install open-jdk for Gitlab to build code and open-jre to run .jar W9**  
sudo apt-get update  
sudo apt-get install openjdk-11-jre openjdk-11-jdk  

**backend: install gitlab-runner on your server and configure it for backend repo W8/W9**  
Install once and configure for back-end.  
Follow one of the class guides and add gitlab-ci.yml to your project  
After installation you should have /home/gitlab-runner package.  
After gitlab-ci build is successful there should be /home/gitlab-runner/api-deployment  
If you experience "sudo: no tty present and no askpass program specified" comment out gitlab-ci.yml sudo service xyz line or look for 
*backend: restart backend service after build W10*

**backend: define backend as linux service W9**    
*We assume that gitlab puts backend jar into /home/gitlab-runner/api-deployment*  
*We assume that jar is called heroes.jar*

cd /etc/systemd/system/  
sudo touch heroes.service

//start-copy  
[Unit]  
Description=dashboard heroes service  
After=network.target  

[Service]  
Type=simple  
User=gitlab-runner  
WorkingDirectory=/home/gitlab-runner/api-deployment    
ExecStart=/usr/bin/java -jar java-heroes.jar  
Restart=on-abort  

[Install]  
WantedBy=multi-user.target  
//stop-copy

Configuration must be reloaded    
sudo systemctl daemon-reload

Process must be enabled    
sudo systemctl enable heroes

Start the service  
sudo service heroes restart  

To check backend use (or check your logfiles)      
sudo service heroes status  

**backend: where is it running W9**  
http://ip_address:port/api_or_other_base_url/rest_url  
http://193.40.155.144:8000/api/heroes  
Some servers (TalTech) don't expose random ports, only 80 and 443. So it will not open anything.

**frontend: install node and yarn for gitlab runner to build frontend code W9**  
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -  
sudo npm install -g yarn  

**frontend: configure gitlab-runner for your frontend repo W9**  
As you installed gitlab runner for backend you only need to configure it.
Follow one of the class guides and add gitlab-ci.yml to your project  
After installation you should have /home/gitlab-runner package.  
After gitlab-ci build is successful there should be /home/gitlab-runner/front-deployment

**frontend: install nginx W9**  
sudo apt-get install nginx  
You should have ip return nginx greeting page: http://193.40.155.144/

**frontend: setup nginx sites-enabled W9**  
go to /etc/nginx/sites-available  
create new config by copying default  
sudo cp default heroes  
go to /etc/nginx/sites-enabled  
make symlink from sites-enabled to sites-available  
sudo ln -s /etc/nginx/sites-available/heroes /etc/nginx/sites-enabled/  
in sites-enabled rm default symlink  
sudo rm default  

**backend: setup nginx to proxy backend W9**  
go to /etc/nginx/sites-available  
sudo nano heroes  
add following code (change port if necessary)  
         
         location /api/ {  
             proxy_pass   http://localhost:8000;  
         }  
sudo service nginx restart  

Backend should be available without port   
http://ip_address/api_or_other_base_url/rest_url  
http://193.40.155.144/api/heroes  

**frontend: setup nginx to return frontend W9**  
create a symlink from gitlab-runner front-end code to /var/www   
sudo ln -s /home/gitlab-runner/front-deployment/ /var/www/front-deployment  

go to /etc/nginx/sites-available  
sudo nano heroes  
change root to point to /var/www/front-deployment  
root /var/www/front-deployment  

sudo service nginx restart  
http://193.40.155.144/ should display your front  

**backend: restart backend service after build W10**  
gitlab-ci.yml should contain: sudo service heroes restart  
Allow gitlab-runner to use sudo when restarting heroes service  
sudo visudo (add to the end)  
gitlab-runner ALL = NOPASSWD: /usr/sbin/service heroes *  

**frontend: url breaks frontend W10**  
Go to /etc/nginx/sites-available  
Edit heroes file, add a rule for nginx to forward to index.html if it doesn't have other files  

    location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }


**backend: install postgresql W9**  
https://computingforgeeks.com/install-postgresql-12-on-ubuntu/  

**backend: postgresql create new database and user W10**  
https://computingforgeeks.com/install-postgresql-12-on-ubuntu/  
change user to postgre
sudo su postgresql  
Open admin sql console:  
psql  
CREATE DATABASE mytestdb;  
CREATE USER mytestuser WITH ENCRYPTED PASSWORD 'mytestuser';  
GRANT ALL PRIVILEGES ON DATABASE mytestdb to mytestuser;  

Command to generate random password:  
< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;  

**backend: connect locally to postgresql db W10**  
port forward local 5555 port to  
ssh -L 5555:localhost:5432 ubuntu@193.40.155.144

Connect as jdbc:postgresql://localhost:5555/mytestdb  
U: mytestuser, P: mytestuser  
//you can also change postgresql conf to allow remote access, but it **won't** work if port 5432 is closed (TalTech) 
https://support.plesk.com/hc/en-us/articles/115003321434-How-to-enable-remote-access-to-PostgreSQL-server-on-a-Plesk-server-  
Then you would connect jdbc:postgresql://193.40.155.144:5555/mytestdb  
U: mytestuser, P: mytestuser  

**backend: connect application to postgresql W10**  
create under /home/gitlab-runner/api-deployment new file
touch custom.yaml (or custom.properties)  
You have to copy all your application.yaml properties into it then you can change sensitive data like db connection (user, password)  
Update heroes.service to use custom.yaml
ExecStart=/usr/bin/java -jar java-heroes.jar --spring.config.location=custom.yaml

**frontend: nginx final configuration W10**  
cd /etc/nginx/sites-available  
heroes file
```bash
server {
    listen       80;
    server_name  localhost;

    root   /var/www/front-deployment;

    location /api/ {
        proxy_pass   http://localhost:8000;
    }

    location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }
}
```

**backend: install mariadb**  
MariaDb is open source version of mysql  
https://linuxize.com/post/how-to-install-mariadb-on-ubuntu-18-04/  

**backend: mariadb create new database and user W10**  
todo some day

**backend: connect locally to mariadb db W10**  
port forward local 5444 port to 
ssh -L 5444:localhost:5432 ubuntu@193.40.155.144

Connect as jdbc:mysql://localhost:5444/mytestdb  
U: mytestuser, P: mytestuser  
