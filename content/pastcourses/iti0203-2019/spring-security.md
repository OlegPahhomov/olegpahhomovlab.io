+++
title = "ITI0203 2019 spring Security"
description = ""
weight = 5
+++

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

## http-basic

* Login is done by the browser
* Users are declared in memory
* No registration

Branches to use

|Back-end | Front-end  | 
|---|---|
|security-httpbasic | master  |


## jwt

* Login is done on the front-end 
* Users are saved to the database
* Registration

|Back-end | Front-end  | 
|---|---|
|security-jwt | security-jwt  |

Some tutorials and links:
* Why and how of Spring Security
    * https://spring.io/guides/topicals/spring-security-architecture
    * https://dzone.com/articles/spring-security-authentication
* Http Basic Baeldung
    * https://www.baeldung.com/spring-security-basic-authentication
* Managing cookies
    * https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?utm_source=chrome-ntp-icon
* JWT: Json web token
    * https://jwt.io/
* Spring Boot + Angular JWT:
    * https://dzone.com/articles/angular-7-spring-boot-jwt-authentication-example
* JWT registration on Angular
    * https://jasonwatmore.com/post/2019/05/22/angular-7-tutorial-part-5-registration-form-user-service
