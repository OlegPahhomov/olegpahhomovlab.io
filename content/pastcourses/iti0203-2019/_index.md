+++
title = "ITI0203 2019"
description = ""
weight = 102
+++

## ITI0203 Information Systems in Java (2019 Autumn)

{{< alert style="danger" >}} Beware! This article was designed for year 2019 edition {{< /alert >}}

### Goal
Learn to write modern application (with separate back-end and front-end). Take it to production.
1) Build simple app
1) Create automated CI/CD pipeline
1) Add spring security

### Introduction
Course runs during 2019 Autumn semester.

### Materials
* [Lecture slides](https://drive.google.com/open?id=1doZRHxeDPWfaZ-lWhGevU9JUFHyDYBLX)
* [Recordings](https://echo360.org.uk/section/aecb285b-cc19-4a84-a224-60628555ab76/public)

### Class code
* [Java Zero to Hero java revision](https://github.com/OlegPahhomov/java-zero-to-hero) 
* [Java-Heroes back-end](https://gitlab.cs.ttu.ee/olpahh/iti0203-2019-heroes-back)
* [Angular-Heroes front-end](https://gitlab.cs.ttu.ee/olpahh/iti0203-2019-heroes-front)

### Setup
1) [Backend](/guides/back-end)
1) [Frontend](/guides/front-end)
1) [Docker](/guides/docker)
1) [AWS](/guides/production)
1) [Machine setup guide](class-machine-setup.md)

### Project
1) [Project](iti0203-2019-project.md)  
