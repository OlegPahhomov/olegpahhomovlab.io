+++
title = "ICS0014 2020 Project"
description = ""
weight = 2
+++

## ICS0014 2020 Projects info

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}


### Introduction
Course is done in teams of 2-4 people. You **must** have a team.  
To pass you need to complete 2 group projects:
1) Java.
1) Backend API application.

### Registering a team
1) Registration opens after declaration deadline
1) Register in excel 
    * excel removed
1) Create a PRIVATE channel (in the lecturer slack) with name team_number_teamname where number is your team number, teamname is your team name (team name is optional)
    1) For example: team_14_souroranges
    1) Important: teams < 10 use 0 before your number, so team_03_architects
        1) It helps with slack order (for me mostly)
1) Invite all your team members to that channel +
    1) @Oleg Pahhomov 

### Project 1: Java basics
##### What are we doing?
It's a test with multiple problems (code and theory). I will share it with you as a zip file on **3rd of March**.
1) One of team members will need to create a private repository for your team using TalTech gitlab.
    1) Add your team mates as maintainers or owners
    1) Add lecturer as reporter (search for user **olpahh**)
1) One of team members has to push the contents of the zip file to your repository
1) All of the team mates have to do the questions, assignments and problems given by the lecturer.
    1) It is up to you how you divide work. 
    1) There are some ways to get disqualified, try to avoid them.
        1) Lecturer wants to see commits from all of the team members.
        1) For more info: [grading excel](https://docs.google.com/spreadsheets/d/1OaBsZOwB_1JT98SPHkXCPre75RAPXrVAwB-Pr_v1b98/edit?usp=sharing)
##### Deadlines:
1) 08 March, Sunday 23:59, each hour delayed is -2% rounded upwards
    * 07 Feb, Saturday 23:59 for 1 day early extra
1) 14 March, Saturday 23:59 **_-30%_**
1) extensions are given only when asked in advance   
##### Submitting
1) You have registered with team
1) You have team chat on slack
1) Write in your team chat:
    ```bash
       @OlegPahhomov we are done. Here is our repository <link to repository>. We have given you reporter access. 
    ```
##### Grading:
1) Follow the grading table [grading excel](https://docs.google.com/spreadsheets/d/1OaBsZOwB_1JT98SPHkXCPre75RAPXrVAwB-Pr_v1b98/edit?usp=sharing)
    * ``https://docs.google.com/spreadsheets/d/1OaBsZOwB_1JT98SPHkXCPre75RAPXrVAwB-Pr_v1b98/edit?usp=sharing``
1) Extras do not affect the max points
1) Each team gets individual feedback
    1) points + comments

### Project 2: Backend API
##### What are we doing?
It's a group project to build backend API with 2 different endpoints. It is built ground up - no zip from lecturer.
All teams will have to do perform different calculations. 

1) You will need to create a NEW private repository for your team using TalTech gitlab.
1) You will need to give me reporter rights.
    1) search for user **olpahh**

##### Theory
A zip will be provided with some java classes. Create a theory package for them. 
Similar to how config folder is done in the class code.

##### Calculator API

Requirements  
1) Calculator API with 2 different GET methods: /calculate1 and /calculate2. 
    1) Both methods have @RequestParam input as List<Integer>
1) Calculator @RestController has minimal logic. Logic happens in CalculatorService @Service and beyond
    1) If you will be using static methods for separate calculations, then it's advisable that they are called from CalculatorService. Meaning they are not inside CalculatorService.
1) Calculator returns CalculationResult.java object (example below)
1) /calculate1 and /calculate2 will have one return in common. I am expecting you not to copy-paste.
1) What to calculate will be given by lecturer, each team will have different things to calculate
1) There should be a total response even though some calculations might return nothing. For example -1,-2 return -1 as maximum, but nothing as positives. So positives should returned null. 
1) Streams are preferred
1) It's up to you whether you declare @RequestParam "input" required or not.
1) Average related calculations are the hardest ones and Integer is not a valid return type there.

Request examples:
```bash
GET /calculator/calculate1?input=1,2,3
GET /calculator/calculate2?input=1,2,3
```

Response object
```java
public class CalculationResult {
    private Integer max;
    private List<Integer> positives;
    // ...and others
    // keep only the properties you need

    //getters and setters below, removed for brevity
}
```

Response example for 1, 2, 3
```json
{
  "max": -1,
  "positives": [
    1,
    2,
    3
  ]
}
```
Response example for -1, -2, -3
```json
{
  "max": -1,
  "positives": null
}
```
or
```json
{
  "max": -1
}
```

    
##### Deadlines:
1) Sunday 23:59, March 29th, each hour delayed is -2% rounded upwards
    * Saturday 23:59 for 1 day early extra
1) Saturday 23:59, April 4th **_-30%_**   
1) extensions are given only when asked in advance
##### Submitting
1) Write in your team chat:
    ```bash
       @OlegPahhomov we are done. Here is our repository <link to repository>. We have given you reporter access. 
    ```
##### Grading:
1) Follow the grading table [grading excel](https://docs.google.com/spreadsheets/d/1OaBsZOwB_1JT98SPHkXCPre75RAPXrVAwB-Pr_v1b98/edit#gid=1037019869)
1) Extras do not affect the max points
1) Each team gets individual feedback
    1) points + comments + grade
##### Defence:
There is no defence this time. Stay safe.

