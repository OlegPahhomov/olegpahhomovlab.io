+++
title = "ICS0014 2020"
description = ""
weight = 301
+++

## ICS0014 Java Technologies 2020 Course info

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

### Goal

In this course:
1) Learn basics of java (latest and greatest)  
2) Build a modern backend API application with Spring Boot

### Introduction
Course runs during first part of 2020 Spring semester.

Course is on slack: https://ics0014-2020.slack.com **_To sign up: https://ics0014-2020.slack.com/join/signup_**  
Slack has links to slides and recordings. It is where I answer your questions and communicate with you.  
Slack is used by businesses around the world. I recommend you create a bookmark folder and put everything from our course there (this is how I roll).

Course is 8 weeks long: there is a 3h lesson each week. Theory and practise are taught together.  
It is advisable to bring your own laptop, however there are also computers supplied by university.  
No lesson on the 25th of February (travelling).  
Course is recorded (success rate ~80%).  

Course is a **pass-fail**. Course is done in teams of 2-4 people.  
To pass you need to complete 2 group projects:
1) Java
1) Backend API application

### Lesson plan
Course is divided into 2: learning java and then building a real modern backend API. You know what they say about plans and reality, so treat this plan with a grain of salt. 
1) Intro, setup. Java objects and functions
1) Java collections and maps
1) Java streams and lambdas
1) Review java
    1) estimated deadline for project 1
1) Spring boot I
1) Spring boot II
1) Spring boot III
1) Review spring boot
    1) estimated deadline for project 2

### Materials
* [Slides](https://drive.google.com/drive/folders/1i0RnEnxcCLAWZ-EaHMxNWMkH39bb_gT2)
    * ``https://drive.google.com/drive/folders/1i0RnEnxcCLAWZ-EaHMxNWMkH39bb_gT2``
    * part 1 java slides are up to date, part 2 will be updated by the end of February 
* [Videos](https://echo360.org.uk/section/ec2ae567-7f22-4b8b-b46f-7234415b7380/public)
    * ``https://echo360.org.uk/section/ec2ae567-7f22-4b8b-b46f-7234415b7380/public``
    * **NB!** After you open it the link changes and new link won't work
* Code
    * [Class code for java - java introduction](https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction)
        * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction``
    * [git practice zip](https://drive.google.com/drive/folders/1i0RnEnxcCLAWZ-EaHMxNWMkH39bb_gT2)
        * ``https://drive.google.com/drive/folders/1i0RnEnxcCLAWZ-EaHMxNWMkH39bb_gT2``
    * [Class review code for java - java zero-to-hero](https://github.com/OlegPahhomov/java-zero-to-hero)
        * ``https://github.com/OlegPahhomov/java-zero-to-hero``
    * [Class code for spring - spring introduction](https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-spring-introduction)
        * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-spring-introduction``


### Setup
What to install during the course. Please refer to [setup](ics0014-2020-setup.md)

### Group projects
Course is done in groups of 2-4 people. I think it is a natural way to learn as it allows you to discuss problems together. Most importantly IT is done in teams.  
Lecturer benefits are more detailed grading (as there is less of it) and less hunting for copy-paste.  
Please refer to [projects](ics0014-2020-project.md) for more info.

### Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.  
(p1 + p2) / (p1max + p2max) * 100  
51+: pass  

### Asking for help
Contact lecturer on slack. Come after the lecture.
For group project related questions and problems use team chats.

### Other courses 
No other courses in Spring semester. (I did ICS0014 last year)  
2019 autumn semester (I can share video lessons if interested)
    1) ICS0024 Automated Testing 
    2) ITI0203 Information Systems in Java.  
