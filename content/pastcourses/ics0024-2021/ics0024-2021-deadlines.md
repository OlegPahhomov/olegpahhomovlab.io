+++
title = "ICS0024 Deadlines"
description = ""
weight = 1
+++
{{< readfile file="/fragments-2021/ics0024/grading-excel.md" markdown="true" >}}

## Project deadlines

### Part 1
[Tested modern application](../ics0024-2021-project/#part-1-tested-modern-application)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/ics0024/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 1 defences
Register with doodle sent in team chats. Info on [defences](../ics0024-2021-project/#defence)

### Part 2
[Selenium and CypressJs](../ics0024-2021-project/#part-2-selenium-and-cypressjs)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/ics0024/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 2 defences
No defence

### Part 3
[Gitlab CI/CD, production](../ics0024-2021-project/#part-3-production-pipeline)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2021/ics0024/deadline-project-3.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 3 defences
Register with doodle sent in team chats. Info on [defences](../ics0024-2021-project/#defence-2)

## Homework deadlines

### Team retro after project 1
This is your 1st graded retro

1. What worked well?
2. What went bad? / What could be improved?
3. What will we commit to improve in the next Sprint?

You can use service like https://easyretro.io/

| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| deadline | Tuesday 19.10 23:59 | for project 1 early birds |
| deadline | Tuesday 26.10 23:59 | for project 1 on time |
| deadline | Tuesday 02.11 23:59 | for project 1 late |

[comment]: <> (### Retro 1)

[comment]: <> ([Team based retro]&#40;../ics0024-2020-class/#team-based-retro&#41;  )

[comment]: <> (Do this retro **after** completing part 1  )

[comment]: <> ({{< table style="table-striped" >}})

[comment]: <> (| deadline      | date        | notes  |)

[comment]: <> (| ------------- |:-------------:| -----|)

[comment]: <> (| deadline 1 | Tuesday, 20.10.2020 23:59 | this deadline is for part 1 early birds |)

[comment]: <> (| deadline 2* | Tuesday, 27.10.2020 23:59 | this deadline is for everyone |)

[comment]: <> ({{< /table >}})

[comment]: <> (*try to have retro before the defence)

[comment]: <> (_works after **deadline** are 0_)

[comment]: <> (### Retro 2)

[comment]: <> ([Team based retro, retro 2 vs retro 1]&#40;../ics0024-2020-class/#team-based-retro-retro-2-vs-retro-1&#41;  )

[comment]: <> (Do this retro **after** completing part 2  )

[comment]: <> ({{< table style="table-striped" >}})

[comment]: <> (| deadline      | date        | notes  |)

[comment]: <> (| ------------- |:-------------:| -----|)

[comment]: <> (| deadline 1 | Tuesday, 17.11.2020 23:59 | this deadline is for part 2 early birds |)

[comment]: <> (| deadline 2 | Tuesday, 24.11.2020 23:59 | this deadline is for everyone |)

[comment]: <> ({{< /table >}})

[comment]: <> (_works after **deadline** are 0_)
