+++
title = "ICS0024 2021"
description = ""
weight = 200
+++
{{< readfile file="/fragments-2021/ics0024/grading-excel.md" markdown="true" >}}


## Goal
1) Automated Testing, write tests:
    1) as develop
    1) as tester 
1) Testing Automation, take back-end API to production and add CI/CD pipeline

## ICS0024 Automated Testing (Autumn 2021)
### Slack
Course is on slack: https://ics0024-2021.slack.com  **To sign up**: https://join.slack.com/t/ics0024-2021/signup  
Slack is for questions, announcements and communication with the lecturer _(important announcements will be sent to email as well)_  
Slack is used by businesses around the world (although lately Teams has been growing faster).

### Structure
Course is 16 weeks long: there is a 1.5h lesson each week. Theory and practise are taught together. Course is divided into 3 parts to match goals.
All lectures will be recorded. 1st part will be focused for on-site, 2nd & 3rd parts are focused for on-line.
It is advisable to bring your own laptop, however there will be computers supplied by university.  
Course is recorded (audio success rate ~90%).  
We will keep a broad overview of what we did in the class info [ICS0024 Class](ics0024-2021-class)

### Grade
Course ends with a **grade**. Course is done in teams of 2-4 people.  
You will be working with the same team on 3 part [project](ics0024-2020-project). Part 2 is standalone, part 1 and 3 work together.
1) Tested Modern Application  
1) Selenium and CypressJs standalone  
1) Testing Automation, CI/CD pipeline (done for first part)  

## Recordings, materials, code

### Recordings
* [Videos](https://echo360.org.uk/section/45bff986-1766-46a2-8eec-ec9475a807ab/public)
  * ``https://echo360.org.uk/section/45bff986-1766-46a2-8eec-ec9475a807ab/public``
  * **NB!** After you open it the link changes and new link won't work
  * Recordings are LIVE only for on-site lectures (as an alternative I could record a video myself)

### Materials
* [Lecture slides](https://drive.google.com/drive/folders/1_QFc_uNB2KLLfuXsGoEVoiV35-m_wiIB?usp=sharing)
    * ``https://drive.google.com/drive/folders/1_QFc_uNB2KLLfuXsGoEVoiV35-m_wiIB?usp=sharing``

### Class code
* [ICS0024-2021 intro code](https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-intro-code)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-intro-code``
* [ICS0024-2021 backend api](https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-backend-api)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-backend-api``
* [ICS0024-2021 Selenium](https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-selenium)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-selenium``
* [ICS0024-2021 Cypress](https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-cypress)
  * ``https://gitlab.cs.ttu.ee/olpahh/ics0024-2021-cypress``
   
### Refresh java
* [git-game.zip](https://drive.google.com/file/d/16eha2T9vuiqbaUq2-TCCXbOUvrN_rDP8/view?usp=sharing)
    * ``https://drive.google.com/file/d/16eha2T9vuiqbaUq2-TCCXbOUvrN_rDP8/view?usp=sharing``
* (Optional) [Java Zero to Hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    * ``https://github.com/OlegPahhomov/java-zero-to-hero``
    * Solutions are on branches solutions-2019 or solutions-2020  
* (Optional) [First time Java guides](/guides/first-time-java)
    * Read through easy guides if Java seems complicated, it gives a brief overview
* (Optional) [Review Java Slides](https://drive.google.com/drive/folders/1g0eB61Cibako_GqarlRV3_coNxrBU935?usp=sharing)
    * ``https://drive.google.com/drive/folders/1g0eB61Cibako_GqarlRV3_coNxrBU935?usp=sharing``
    * Read through slides if Java seems complicated, it gives a brief overview
* (Optional) [Java basics - ICS0014-2021-java](https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java``
    * Clone this if Java seems complicated, it has some good examples and explanations 
    * Solutions are on a branch solutions-xyz  


### Setup
For course part 1
1) [git](/guides/git)
1) [back-end](/guides/back-end)

For course part 3
1) [docker](/guides/docker)
1) [production](/guides/production)
1) class machine (will be added in part 3)

### Child pages
{{< childpages >}}
