+++
title = "ICS0024 Mentors"
description = ""
weight = 100
+++
{{< readfile file="/fragments-2021/ics0024/grading-excel.md" markdown="true" >}}

## Lecturer

{{< table style="table-striped" >}}
| name          | gitlab user   | role  |
| ------------- |-------------| -----|
| Oleg Pahhomov | olpahh        | lecturer |
{{< /table >}}

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}} 
removed
{{< /code >}}
