+++
title = "ICS0014 2021 Project"
description = ""
weight = 2
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1EOqbHgg3hStWmqlY8xkJBci_33Rn4e0kxt42nUTNQ1c/edit?usp=sharing)

## Introduction
Course is done in teams of 2-4 people. You **must** have a team.  
To pass you need to complete 2 group projects:
1) Java
1) Backend API

This article is about projects, for general info refer to [ICS0014 2021](../..)

## Registering a team
{{< readfile file="/fragments-2020/ics0014/register.md" markdown="true" >}}


## Project 1: Java
### What are we doing?
It's a test with multiple problems (code and theory). I will share it with you as a zip file [ics0014-test1.zip](https://drive.google.com/drive/folders/1RQGanycvk3gajIIZMgiWn2PcKPztpUg5).
1) One of team members will need to create a private repository for your team using TalTech gitlab.
   1) Add your teammates as maintainers or owners
   1) Add the lecturer as reporter (search for user **olpahh**)
1) One of team members has to push the contents of the zip file to your repository
1) All the teammates have to do the questions, assignments and problems given by the lecturer.
   1) It is up to you how you divide work.
   1) There are some ways to get disqualified, try to avoid them.
      1) Lecturer wants to see commits from all the team members.
      1) More info in grading

### Deadlines:
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Submitting
1) You have registered a team
1) You have team chat on course Slack
1) Write in your team chat:
    ```bash
       @OlegPahhomov we are done. Here is our repository <link to repository>. We have given you reporter access. 
    ```
   
### Grading:
{{< readfile file="/fragments-2020/ics0014/grading-1.md" markdown="true" >}}

## Project 2: Backend API
### What are we doing?
Our goal is to connect java exercises from part 1 to an actual production-ready backend API.  
Lessons (5-x) will clarify what do those words mean and how to implement them into practice.

It's a group project to build a backend API with 2 different endpoints. It is built ground up - no zip from lecturer.
Exact calculations will be shared in team chats. 

1) One of team members will need to create a (new) private repository for your team using TalTech gitlab.
   1) Add your teammates as maintainers or owners
   1) Add the lecturer as reporter (search for user **olpahh**)

P.S Theory will be given separately. Add it to your project. Add it as a separate package for your project. 
```bash
ee.taltech.calculator.theory
```

### Calculator API
Requirements

1) Calculator API must have 2 different GET methods: /calculate1 and /calculate2
   1) Both methods have @RequestParam input as a List of Integers
   ```java
   (@RequestParam List<Integer> input)
   ```
   which translates to following requests
   ```bash
   GET /calculator/calculate1?input=1,2,3
   GET /calculator/calculate2?input=1,2,3
   ```
1) Each calculator endpoint (calculate1, calculate2) has to do 3 calculations (6 in total). 
   1) Grading reflects this well [link](https://docs.google.com/spreadsheets/d/1EOqbHgg3hStWmqlY8xkJBci_33Rn4e0kxt42nUTNQ1c/edit#gid=1039301547)
1) Each calculator endpoint returns response as json
   ```json
   {
      "calculation-1": "calculation-1-result",
      "calculation-2": "calculation-2-result",
      "calculation-3": "calculation-3-result"
   }
   ```
   So if you had to find max, min and positives you could return
   ```json
   {
      "max": 3,
      "min": 1,
      "positives": [1,2,3]
   }
   ```
   Json is achieved by returning a java object like this
   ```java
   public class CalculationResult {
      private Integer max;
      private Integer min;
      private List<Integer> positives;
   
      //getters and setters hidden for brevity
   }
   ```
1) I expect you to have structure in your project.
   1) Controllers have minimal logic. (annotated by @RestController)
   1) All logic happens inside a Service. (annotated by @Service)
   1) (optional) I recommend having separate class for each of the necessary calculations. That way you can keep those functions static and create unit tests for them.
1) Return nulls when calculation doesn't return a result. E.g. there are no negatives in (1,2,3)
   ```json
   {
      "max": 3,
      "negatives": null
   }
   ```
   or nulls can be omitted (check out class code)
   ```json
   {
      "max": 3
   }
   ```
1) Streams are preferred
1) Averages should be reasonably rounded (1-2 decimal places), Integer is not a valid return type

### Deadlines:
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Submitting
1) Write in your team chat:
    ```bash
       @OlegPahhomov we are done. Here is our repository <link to repository>. We have given you reporter access. 
    ```

### Grading:
{{< readfile file="/fragments-2020/ics0014/grading-2.md" markdown="true" >}}

### Defence:
After you've submitted, and I've graded you will receive a defence doodle. All team must attend the defence.  
Defence is done in university's Teams. When it's your time prepare a call with your team and invite me. Webcam is mandatory. 
Teams can be installed on the phone

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2) / (p1max + p2max) * 100  
```  
{{< table style="table-striped" >}}
| percentage      | grade        |
| ------------- |:---------------:|
| 51+   | pass |
| 0-50  | fail |
{{< /table >}}
