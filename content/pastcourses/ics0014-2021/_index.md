+++
title = "ICS0014 2021"
description = ""
weight = 300
+++

## ICS0014 Java Technologies 2021 Course info


### Goal

In this course:
1) Learn basics of java (latest and greatest)  
2) Build a modern backend API application with Spring Boot

### Introduction
Course is on slack: https://ics0014-2021.slack.com  **To sign up**: https://ics0014-2021.slack.com/join/signup  
Slack is where I answer your questions and communicate with you.  
Slack is used by businesses around the world (although lately Teams has been growing faster).  
I recommend you create a bookmark folder and put everything from our course there (this is how I roll).

Course is 8 weeks long: there is a 3h lesson each week. Theory and practise are taught together.  
It is advisable to bring your own laptop, however there are also computers supplied by university.
Course is recorded (audio success rate ~95%).

Course ends with a **grade**. Course is done in teams of 2-4 people.  
To pass you need to complete 1 [project](ics0014-2021-project) which is graded in 2 parts:
1) Java
1) Backend API

### Materials
* [Slides](https://drive.google.com/drive/folders/1RQGanycvk3gajIIZMgiWn2PcKPztpUg5?usp=sharing)
  * ``https://drive.google.com/drive/folders/1RQGanycvk3gajIIZMgiWn2PcKPztpUg5?usp=sharing``
* [Videos](https://echo360.org.uk/section/bae0eef9-3e52-4300-b233-867086bf68fe/public)
  * ``https://echo360.org.uk/section/bae0eef9-3e52-4300-b233-867086bf68fe/public``
  * **NB!** After you open it the link changes and new link won't work
* [Class code for java](https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java)
  * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-java``
* [Class review code for java - java zero-to-hero](https://github.com/OlegPahhomov/java-zero-to-hero)
  * ``https://github.com/OlegPahhomov/java-zero-to-hero``
* [Class code for spring boot](https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-spring)
  * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2021-spring``

### Setup
What to install during the course. Please refer to [setup](ics0014-2021-setup)

### Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2) / (p1max + p2max) * 100  
```  
{{< table style="table-striped" >}}
| percentage      | grade        | 
| ------------- |:---------------:| 
| 51+   | pass | 
| 0-50  | fail | 
{{< /table >}}

### Asking for help
Contact lecturer on Slack. Come after the lecture. For group project related questions and problems use team chats.

### Other courses 
No other courses in Spring semester. 
2020 autumn semester (I can share video lessons if interested)  
1) ICS0024 Automated Testing 
1) ITI0203 Information Systems in Java.  
