+++
title = "ICS0014 2021 Deadlines"
description = ""
weight = 1
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1EOqbHgg3hStWmqlY8xkJBci_33Rn4e0kxt42nUTNQ1c/edit?usp=sharing)


## Project deadlines

### Part 1
[Java](../ics0014-2021-project/#project-1-java)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 2
[Backend API](../ics0014-2021-project/#project-2-backend-api)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Defence
Online university teams defence after Part 2 is graded.

## Homework deadlines

### HW1
[Retro 1](/guides/retro/#homework---retro)  
Do this retro **after** completing part 1  
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-homework-1.md" markdown="true" >}}
{{< /table >}}
_works after **deadline** are 0_

### HW2
[Retro 2](/guides/retro/#homework---2nd3rd-retro-1-question)  
Do this retro **after** completing part 2  
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/ics0014/deadline-homework-2.md" markdown="true" >}}
{{< /table >}}
_works after **deadline** are 0_
