+++
title = "ITI0203 2020"
description = ""
weight = 101
+++

[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)

## ITI0203 Information Systems in Java (2020 Autumn)

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

### Goals
1) Write modern application (with separate back-end and front-end). 
1) Take it to production and add CI/CD pipeline.
1) Secure it with spring security.

### Introduction
Course is on slack: https://iti0203-2020.slack.com  **To sign up**: https://iti0203-2020.slack.com/join/signup  
Slack is where we answer your questions and communicate with you.  
Slack is used by businesses around the world (although lately Teams has been growing faster).  
I recommend you create a bookmark folder and put everything from our course there (this is how I roll).

Course is 16 weeks long: there is a 3h lesson each week. Theory and practise are taught together.  
It is advisable to bring your own laptop, however there are also computers supplied by university.  
Course is recorded (audio success rate ~90%).  

Course ends with a **grade**. Course is done in teams of 2-4 people.  
To pass you need to complete 1 [project](iti0203-2020-project) which is graded in 3 parts:
1) Simple Modern Application
1) Gitlab CI/CD, production
1) Securing with Spring, finalizing application

### Mentor
Each team gets a [mentor](mentors).  
Mentor stays with one team during whole course.

### Materials
* [Slides](https://drive.google.com/drive/folders/1n5Tbc82lEG8HLT3VO9VGbKbbmCrvG984)
    * ``https://drive.google.com/drive/folders/1n5Tbc82lEG8HLT3VO9VGbKbbmCrvG984``
* [Videos](https://echo360.org.uk/section/fa11c2dc-2eb2-4f7d-bd29-d98befb36005/public)
    * ``https://echo360.org.uk/section/fa11c2dc-2eb2-4f7d-bd29-d98befb36005/public``
    * **NB!** After you open it the link changes and new link won't work

### Class code
* [Java-Heroes back-end](https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-back)
    * ``https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-back``
* [Angular-Heroes front-end](https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-front)
    * ``https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-front``

### Refresh java
* git-practice.zip
    * Inside Slides -> review 
* [Java Zero to Hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    * ``https://github.com/OlegPahhomov/java-zero-to-hero``
    * Solutions are on branches solutions-2019 or solutions-2020  
* (Optional) Review Slides (Slides -> review)
    * Read through slides if Java seems complicated, it gives a brief overview
* (Optional) [Java basics - ICS0014-2020-java-introduction](https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction)
    * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction``
    * Clone this if Java seems complicated, it has some good examples and explanations 
    * Solutions are on a branch solutions-2020  

### Setup
1) [back-end](/guides/back-end)
1) [git](/guides/git)
1) [front-end](/guides/front-end)
1) [docker](/guides/docker)
1) [production](/guides/production)
1) [class machine](iti0203-2020-production)

### Child pages
{{< childpages >}}
