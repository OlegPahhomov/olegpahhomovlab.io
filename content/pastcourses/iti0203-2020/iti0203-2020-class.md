+++
title = "ITI0203 Class"
description = ""
weight = 400
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

## Spoiler
Here we will write what we did in the class.

## Homework 3
### Team based retro, retro 3 vs retro 2
Submit in team chat a retro with following content. Retro has to be digital: gitlab pages, gitlab wiki or google doc.

A lot of teams were making their retros fun by using: https://funretro.io/

https://www.scrum.org/resources/what-is-a-sprint-retrospective
* What worked well?
* What went bad? / What could be improved?
* What will we commit to improve in the next Sprint?
* Compare retro 3 vs retro 2. What is the progress?

## 12th lesson 2020-11-16
TBD

## 11th lesson 2020-11-09 [End of part 2] [Start of part 3]
...

## 10th lesson 2020-11-02
...

## 9th lesson 2020-10-26
...

## Homework 2
### Team based retro, retro 2 vs retro 1
Submit in team chat a retro with following content. Retro has to be digital: gitlab pages, gitlab wiki or google doc.

A lot of teams were making their retros fun by using: https://funretro.io/

https://www.scrum.org/resources/what-is-a-sprint-retrospective
* What worked well?
* What went bad? / What could be improved?
* What will we commit to improve in the next Sprint?
* Compare retro 2 vs retro 1. What is the progress?

## 8th lesson 2020-10-19
...

## 7th lesson 2020-10-12 [Start of Part2]
We take a breather from our project and start installing it on the server

### Theory
Server, production, buildserver, CI/CD

### Practise
Individual practise
1. setting up aws server
    1. production folder

## 6th lesson 2020-10-05 [End of Part1]
Go over points & answer team questions

## 5th lesson 2020-09-28

### Theory
Testing, database (slides 11, 8, 10).

### Practise
Individual practise.
1. Testing, code coverage. TestRestTemplate, Mockmvc
1. Database. Linking tables. Hero + equipment.
1. Docker, postgresql

Retro about our class. (if we have time)

Project questions and help (last 40 min)

## 4th lesson 2020-09-21

### Theory
Connecting front and back. 
Guest speaker about analysis. Slides:
* ENG https://docs.google.com/presentation/d/1UXQobfR9t2XNNBHeq_lecBsQFGkfgt40VhlbCaYU1E0/edit?usp=sharing
* EST https://docs.google.com/presentation/d/1ratLCmODiAup8O8GNpl_w8FBrO3xssD80h8U_PHP1FE/edit?usp=sharing

### Practise
Individual practise. Connecting front and back.
1. https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-front
1. https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-back  

Project questions and help (last 40 min)

Future topics in random order:
* testing
* multiple tables and relationships
* install docker
* connecting to external API

## Homework 1
### Team based retro
Submit in team chat a Google doc with following content:

https://www.scrum.org/resources/what-is-a-sprint-retrospective
* What worked well?
* What went bad? / What could be improved?
* What will we commit to improve in the next Sprint?

More information from the class or mentors

## 3rd lesson 2020-09-14

### Theory
Javascript frontend (Slide nr 9)

### Practise
Individual practise
1. Clean up back-end project so copying it won't cause deductions
    1. Add service and move logic there
        1. controller <-> service <-> repository
    1. Replace Heroes controller methods to use db
1. Setup git to track class code
    1. https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-front
    1. [class-git](/guides/git/class-git)
1. Install node & angular
    1. [angular](/guides/front-end/angular)
1. We will follow Angular Heroes guide 
    1. https://angular.io/tutorial
        1. Only part completed is "create a new workspace and an initial application"
    1. All chapters
    1. After we are done with the guide we will connect to back-end (if not now then next lesson)  

Team practise [30min before the end]
1. Let's plan first sprints
    1. [mentors](../mentors)
    1. Mentor registration: https://doodle.com/poll/kbuh8ayh2dk4v9fi

## 2nd lesson 2020-09-07

### Theory
Spring Boot, HTTP and API (Slide nr 6.0, 6.1, 6.2)

### Practise
Individual practise
1. Setup git to track class code
    1. https://gitlab.cs.ttu.ee/olpahh/iti0203-2020-heroes-back
    1. [class-git](/guides/git/class-git/)
1. Follow along and complete ongoing todo's
    1. Index/Greeting controller
    1. Swagger
    1. (skipped) Testing: MockMvc, TestRestTemplate, RestAssured
    1. API controller
    1. H2 database, spring data, hibernate
        1. will review in the future, went really fast

## 1st lesson 2020-08-31

### Theory
Introductions and quick Java review. (Slide nr 0, 1)

### Practice
1. Install everything for backend:
    - [back-end](/guides/back-end)
    - _once done move to next step_
1. Git game
    - Groups of **2** people
    - [Course home](../..) -> Materials -> Slides -> Review -> git-practise.zip
    - Open it with IntelliJ
    - Follow README.md
    - Later follow ReadyPlayerXyz.java
        - _there are no right or wrong answers, the code should compile and run_
    - _once done move to next step_    
1. Java zero to hero
    - Individual work
    - Clone [https://github.com/OlegPahhomov/java-zero-to-hero](https://github.com/OlegPahhomov/java-zero-to-hero)
    - There are todos scattered all over the code, levels 0 to 15
        - _there are solutions on branches solutions-2019 and solutions-2020_ 
    - _once done move to next step_
1. Project part 1 starts next week
    - Get a team (and topic?)
    - Review [project guide](../iti0203-project) and grading excel

