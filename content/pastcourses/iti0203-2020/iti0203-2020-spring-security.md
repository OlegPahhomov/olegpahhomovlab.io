+++
title = "ITI0203 Spring Security"
description = ""
weight = 500
+++

## Spring Security

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

Spring Security is a powerful and highly customizable authentication and access-control framework. 
It is the de-facto standard for securing Spring-based applications.

Spring Security is a framework that focuses on providing both authentication and authorization to Java applications. 
Read more: https://spring.io/projects/spring-security

Read about the why and how of Spring Security
    * https://spring.io/guides/topicals/spring-security-architecture

For our projects Spring Security can be integrated in different ways. 
For our project we will focus on 2 implementations:
* **http-basic** with sessions held and managed by backend application
    * outdated, but easy
* **jwt** with sessions help by the frontend and managed in the database 
    * more secure, but more complex
    
Our grading encourages you to use jwt.


## Introduction

## http-basic

* Login is done by the browser
* Users are declared in memory
* No registration

Branches to use

|Back-end | Front-end  | 
|---|---|
|feature/http-basic*|main|

_*also in the main_ as of Nov 27. Might change so use feature branch instead

## jwt

* Login is saved on the front-end 
* Users are saved to the database
* Registration (optional)

|Back-end | Front-end  | 
|---|---|
|feature/jwt|feature/jwt|

## Tutorials and Links

* Why and how of Spring Security
    * https://spring.io/guides/topicals/spring-security-architecture
    * https://dzone.com/articles/spring-security-authentication
    * https://spring.io/blog/2013/07/03/spring-security-java-config-preview-web-security/
* Http Basic Baeldung
    * https://www.baeldung.com/spring-security-basic-authentication
* Managing cookies
    * https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?utm_source=chrome-ntp-icon
* JWT: Json web token
    * https://jwt.io/
* Spring Boot + Angular JWT:
    * https://dzone.com/articles/angular-7-spring-boot-jwt-authentication-example
* JWT registration on Angular
    * https://jasonwatmore.com/post/2019/05/22/angular-7-tutorial-part-5-registration-form-user-service
