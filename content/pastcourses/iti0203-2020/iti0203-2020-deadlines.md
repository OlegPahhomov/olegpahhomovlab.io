+++
title = "ITI0203 Deadlines"
description = ""
weight = 2
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)

## Project deadlines

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}


### Part 1
[Simple modern application](../iti0203-2020-project/#part-1-simple-modern-application)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 1 defences
Register with doodle sent in team chats. Defences happen for 3 weeks during 1st part of Monday lesson.

### Part 2
[Gitlab CI/CD, production](../iti0203-2020-project/#part-2-gitlab-cicd-production)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 2 defences
No defence

### Part 3
[Securing with Spring, finalizing application](../iti0203-2020-project/#project-part-3-securing-with-spring-finalizing-application)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-3.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Part 3 defences
Register with doodle sent in team chats. Defences are happening on 14.12.2020 Monday, 05.12.2021 Tuesday, 11.12.2021 Monday

## Homework deadlines

### Retro 1
[Team based retro](../iti0203-2020-class/#team-based-retro)
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| deadline | Tuesday, 29.09.2020 23:59 |  |
{{< /table >}}
_works after **deadline** are 0_

### Retro 2
[Team based retro, retro 2 vs retro 1](../iti0203-2020-class/#team-based-retro-retro-2-vs-retro-1)  
Do this retro **after** completing part 1  
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| deadline 1 | Tuesday, 20.10.2020 23:59 | this deadline is for part 1 early birds |
| deadline 2 | Tuesday, 27.10.2020 23:59 | this deadline is for everyone |
{{< /table >}}
_works after **deadline** are 0_

### Retro 3
[Team based retro, retro 3 vs retro 2](../iti0203-2020-class/#team-based-retro-retro-3-vs-retro-2)  
Do this retro **after** completing part 2  
{{< table style="table-striped" >}}
| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| deadline 1 | Tuesday, 24.11.2020 23:59 | this deadline is for part 2 early birds |
| deadline 2 | Tuesday, 01.12.2020 23:59 | this deadline is for everyone |
{{< /table >}}
_works after **deadline** are 0_
