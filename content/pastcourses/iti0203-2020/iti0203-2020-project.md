+++
title = "ITI0203 Project"
description = ""
weight = 2
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)

## Introduction

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

Course is done in teams of 2-4 people. You **must** have a team. Solo teams are not allowed. There is much to gain by working together.  

To pass you need to complete 1 project which is graded in 3 parts:
1) Simple modern application
1) Gitlab CI/CD, production
1) Securing with Spring, finalizing application

### Registering a team
1) Register in excel
   * excel removed
1) Create a **private** channel with name team_number_teamname where number is your team number, 
teamname is your team name (team name is optional)
    1) For example: team_14_happyoranges
    1) Important: teams < 10 use 0 before your number, so team_03_funnyarchitects
        1) It helps with slack order (for us)
    1) Create channel in iti0203-2020 slack, no need for separate slack
1) Invite all your team members and all [mentors](../mentors) to that channel

### Topic
You will work on the same topic for the whole course.
Pick your own topic or use one of [lecturer topics](https://docs.google.com/document/d/1gRZVxAJq5MU_cqml38r1PgQtmgRdRmoDZfQF0bgCJMk/edit?usp=sharing).  

We have written some guides for [external-api](/guides/external-api), 
you can get more ideas using [rapidapi](https://rapidapi.com/).
  
If you use an external API then it must be connected through your back-end not your front-end. Using external APIs is encouraged. 
You can connect google maps for front-end (but not as many points).

Building browser-focused games is not allowed, because we want to develop back first, front second. 

## Part 1 "Simple modern application"

Start working with your team and with your code.  

### Goal
Build a simple modern application.
Backend, frontend, git.

### What are we doing?
1. Get team, mentor, topic, slack channel and create repositories
1. In a repo of your choice create a wiki page with an overview of what your team will do.
    * Example:[Time tracking webpage](https://github.com/kilpkonn/gtm-enhanced/blob/master/readme/business-analysis.md)
1. Create a backlog of at least 10 business issues. Write them as user stories.
    * [User stories description](https://www.mountaingoatsoftware.com/agile/user-stories)  
    * Example: [2019 class project user stories](https://docs.google.com/document/d/1sgqeR2vZ10mQ_UJe4qtD-tNOuQbvmCIaOXIzjB4n724/edit?usp=sharing)
1. Select 5 of the most important issues for development.
1. At the end of development period you should have a working application
1. Follow [Grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing) for details and guidence

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

* In real world it is analogous to a production deadline. Marketing is ready, all demos and events are booked and IT team must deliver.
    * If you deliver early, life is good
* If you need an extension and have a valid reason(s), contact us. 
    * Extension is not given on the last day.
    * In 2019 some teams were involved with army training

### Submitting
* Team must have a team chat with all of the team members and [mentors](../mentors)
    * We use team chat for communication and help. Contact directly only on personal matters.
    * Using team chats enables us to identify and tackle common problems.
* Teams must use use taltech gitlab (gitlab.cs.ttu.ee)
    * Add lecturer and all mentors to your gitlab project **as reporter**
        * usernames are in [mentors](../mentors)
    * No access to repository is treated the same as no submission
* Team must confirm with the lecturers that they can access the code
    * As there are 30 teams we are not able to connect email invitations with teams. 
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Here is our back-end repository <link to repository>. We have given you reporter access. 
    Here is our front-end repository <link to repository>. We have given you reporter access. 
    Here is our wiki/pages. We have given you access. For wiki we have home page in wiki (it fixes access problems).
    We have read books. Person A has read Xyz, Person B has read Xyz2.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
{{< readfile file="/fragments-2020/iti0203/grading-1.md" markdown="true" >}}

### Defence
{{< readfile file="/fragments-2020/defence.md" markdown="true" >}}

## Part 2 "Gitlab CI/CD, production"

Continue working with your team and with your code.  

### Goal
Commit to main/master branch triggers gitlab CI/CD pipeline. 
Code is compiled, tested, deployed to the server, application is restarted.  

### What are we doing?
* Your team needs an empty virtual server. Pick one: AWS, Azure, Digitalocean etc.
    * goodies: https://education.github.com/pack
* You need to setup gitlab CI/CD setup for backend and frontend repositories.
* You need to setup production
    * Backend is running on the server as a linux service
    * Frontend application served by web server (Nginx/Apache)
    * Backend is proxied by web server
    * Or use some combination of docker to replace one (or all) of the above
* You need to create a detailed guide about all of the above 
* For more details: [grading excel part 2](https://docs.google.com/spreadsheets/d/19OaDYavBg7YLNa3cyCj5GVvd-Tk8lqWLi3-84_FU-yw/edit#gid=1231401474)
    * There are tons of extras!

### A component diagram
Create a component diagram of your **actual** setup. Create your own picture as using mine is not allowed.   
<img src="../component_diagram.png">
Write a description.
{{< panel title="Component diagram" style="secondary" >}} 
Production is set up using AWS EC2. EC2 is an empty Ubuntu 20.04x virtual machine. Server was configured by hand.
For installation guide please follow to [link to installation guide].
Gitlab ci is used for build and deploy process. It is configured to test all branches and deploy the main branch. 
Gitlab runner, production and database are installed on the server.
For gitlab runner shell installation is used. Gitlab builds jar file for backend and dist files for frontend.
Backend is setup as linux process. Backend configuration is in external .yaml file. Nginx is used to serve frontend files. 
Backend is connected to frontend on nginx using reverse proxy. For database h2 is used. 
{{< /panel >}}

### Alternative technologies
{{< readfile file="/fragments-2020/iti0203/alternatives.md" markdown="true" >}}

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Submitting
* For grading we will commit to backend and frontend repositories expecting front-end and back-end production to change 
    * For that to work update us from **reporter** to **maintainer** 
    * Please confirm that we can push
* We also need access to your production server to validate your configuration
    * [mentors](../mentors) has our ssh keys
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here is our back-end repository <link to repository>. We have given all mentors maintainer access. 
    Here is our front-end repository <link to repository>. We have given all mentors maintainer access.
    Here is our wiki/pages <link to wiki/pages>. All mentors can access. For wiki we have home page in wiki (it fixes access problems).
    Here is our installation guide <link to guide>. All mentors can access. 
    Here is our component diagram & description <link to both>. All mentors can access. 
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)

### Grading
{{< readfile file="/fragments-2020/iti0203/grading-2.md" markdown="true" >}}

### Defence
* There is no defence
    * We think committing to your repository and seeing what happens is enough

### Tips
{{< readfile file="/fragments-2020/cicd-tips.md" markdown="true" >}}


## Project part 3 "Securing with Spring, finalizing application"
Continue working with your team and with your code

### Goal
Secure application with Spring Security. Finalize. Fix bugs. Improve design.

### What are we doing?
* We are working on our Application again.
* We are adding Spring Security for registration and roles (admin/user).
* We are fixing our bugs so application works as intended.
* We are adding angular-material so application looks nice.
* We are adding some daemon processes like synchronization because we can.

### Deadlines
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2020/iti0203/deadline-project-3.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

_*This deadline is for people who would like to leave for winter holidays. Experience has shown grading is slow._

### Submitting
* **When submitting** write in your team chat:
    {{< code lang="bash" >}} 
    @channel we are done. 
    Our application runs <link or ip>.
    To connect to our server use <ip>. We have added your ssh keys to the server.
    Here are our test users: admin (username/password), user (you can register or username/password).
    Here are our links:    
    Here is our back-end repository <link to repository>. We have given you maintainer access. 
    Here is our front-end repository <link to repository>. We have given you maintainer access.
    Here is our wiki/pages. We have given you access. For wiki we have home page in wiki (it fixes access problems).
    Here are our user stories <link to stories>. 
    Here is our business analysis <link to business analysis>.
    Here is our development story <link to development story>.
    We have completed some extras from part 1. Please grade (name stories).
    We have read books. Person A has read Xyz, Person B has read Xyz2.
    {{< /code >}} 
    * Not notifying is treated the same as no submission. How else would lecturers know that you are finished?
    * Thanks! For it makes a huge difference to have all links at once. 
    There are 30 teams and we still have no [neuralink](https://techcrunch.com/2020/07/09/elon-musk-sets-update-on-brain-computer-interface-company-neuralink-for-august-28/)
* While it is not mandatory, it is recommended to submit together with production as it speeds up grading.

### Grading
{{< readfile file="/fragments-2020/iti0203/grading-3.md" markdown="true" >}}

### Defence
{{< readfile file="/fragments-2020/defence.md" markdown="true" >}}

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2 + p3) / (p1max + p2max + p3max) * 100  
```  

{{< table style="table-striped" >}}
| percentage      | grade        | description  |
| ------------- |:---------------:| -----|
| 91-100 | 5 | excellent |
| 81-90  | 4 | very good |
| 71-80  | 3 | good |
| 61-70  | 2 | satisfactory |
| 51-60  | 1 | sufficient |
{{< /table >}}

[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)
