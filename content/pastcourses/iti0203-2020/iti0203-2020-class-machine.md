+++
title = "ITI0203 Class Machine"
description = ""
weight = 401
+++
[The grading excel](https://docs.google.com/spreadsheets/d/1bB7354ZW1Bqivcvne5Uc77HLEXPUuiMa4Taj9rrs0lE/edit?usp=sharing)

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}

{{< alert style="warning" >}} This guide is intentionally minimalistic. For it is your goal to write a proper guide yourself. {{< /alert >}}

This guide is intentionally minimalistic.
For it is your goal to write a proper guide yourself.


## Initial setup
You have obtained the machine.  
I am using ubuntu 20.04  
All commands are done on the server


## connecting 
ssh -i ".ssh/olegoleg-t480s.pem" ubuntu@ec2-13-48-6-79.eu-north-1.compute.amazonaws.com
ssh ubuntu@ec2-13-48-6-79.eu-north-1.compute.amazonaws.com

## virtual memory
Aka we can survive angular build

Running htop will show how much memory you have.
https://itsfoss.com/create-swap-file-linux/  
For 2Gb virtual memory
```bash
sudo fallocate -l 2G /swapfile  
sudo chmod 600 /swapfile  
sudo mkswap /swapfile  
sudo swapon /swapfile  
sudo swapon -show  
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
```

htop to check restart server and check whether virtual memory change was permanent sudo reboot

## install dependencies
sudo apt-get update  
sudo apt-get upgrade  
sudo apt-get install openjdk-11-jre openjdk-11-jdk  

## gitlab runner
use class guide  
use amd64  
comment out some gitlab runner file
https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26605  

## test your jar file
run jar file locally
or on the server
with java -jar jarfile.jar

## system process
cd /etc/systemd/system/
sudo touch heroes.service

File contents:
```bash
[Unit]
Description=dashboard heroes service
After=network.target

[Service]
Type=simple
User=gitlab-runner
WorkingDirectory=/home/gitlab-runner/api-deployment
ExecStart=/usr/bin/java -jar java-heroes.jar
Restart=on-abort

[Install]
WantedBy=multi-user.target
```


Configuration must be reloaded
sudo systemctl daemon-reload

Process must be enabled
sudo systemctl enable heroes

Start the service
sudo service heroes restart

To check backend use (or check your logfiles)
sudo service heroes status

## Allow gitlab runner to restart backend
as ubuntu user type sudo visudo  
Add to the end following line:
gitlab-runner ALL = NOPASSWD: /usr/sbin/service heroes *

## Where is it running at
http://ip_address:port/api_or_other_base_url/rest_url
http://13.48.6.79:8080/api/

## Frontend
Install node on linux
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

## Nginx
sudo apt-get install nginx
http://13.48.6.79/

## Nginx - replace default config with yours
go to /etc/nginx/sites-available
create new config by copying default
sudo cp default heroes
go to /etc/nginx/sites-enabled
make symlink from sites-enabled to sites-available
sudo ln -s /etc/nginx/sites-available/heroes /etc/nginx/sites-enabled/
in sites-enabled rm default symlink
sudo rm default

## Backend proxy
Add to nginx file
```bash
    location /api/ {  
        proxy_pass   http://localhost:8000;  
    }  
```
Both http://13.48.6.79:8080/api/ and http://13.48.6.79/api/ work for backend

## Show your frontend
create a symlink from gitlab-runner front-end code to /var/www
sudo ln -s /home/gitlab-runner/front-deployment/ /var/www/front-deployment

go to /etc/nginx/sites-available
sudo nano heroes
change root to point to /var/www/front-deployment
root /var/www/front-deployment

sudo service nginx restart
http://13.48.6.79/ should display your front

## Frontend urls break
Edit heroes file, add a rule for nginx to forward to index.html if it doesn’t have other files
Replace old location /

```bash
    location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }
```

## final nginx file

```bash
server {
    listen       80;
    server_name  localhost;

    root   /var/www/front-deployment;

    location /api/ {
        proxy_pass   http://localhost:8080;
    }

    location / {
        index  index.html index.htm;
        if (!-e $request_filename){
          rewrite ^(.*)$ /index.html break;
        }
    }
}
```
