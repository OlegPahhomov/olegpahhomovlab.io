+++
title = "ITI0203 Mentors"
description = ""
weight = 400
+++


## Mentors

{{< alert style="danger" >}} Beware! This article was designed for year 2020 edition {{< /alert >}}


{{< table style="table-striped" >}}
| name          | gitlab user   | role  | angular | react | vue |  aws | azure | kotlin |
| ------------- |-------------  | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| Oleg Pahhomov | olpahh        | lecturer | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Ilja Samoilov | ilsamo        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Tavo Annus    | taannu        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> |
| Marten Jürgenson | mjurge     | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> |
| Oskar Pihlak  | ospihl        | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| Enrico Vompa  | envomp        | mentor   | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| Timo Loomets  |  tiloom       | mentor   | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
| Tanel Ehatamm |  tanel.ehatamm       | mentor   | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-times"></i> |
{{< /table >}}

## ssh keys
It is easier just to add all keys

{{< code lang="bash" >}} 
removed
{{< /code >}}
