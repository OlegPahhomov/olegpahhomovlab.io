+++
title = "Docker examples"
description = ""
weight = 10
+++

## Docker-compose
We will use docker-compose for running applications locally.

create file **docker-compose.yaml** with following content
{{< code lang="bash" >}} 
version: "3.3"
services:
  db:
    image: postgres:11
    ports:
      - 5632:5432
    environment:
      - POSTGRES_USER=root
      - POSTGRES_PASSWORD=root
{{< /code >}}

To start run on the command line (same folder you have docker-compose file in)
{{< code lang="bash" >}} 
docker-compose up
{{< /code >}}

Detached mode 
{{< code lang="bash" >}} 
docker-compose up -d
{{< /code >}}

To stop
{{< code lang="bash" >}} 
docker-compose down
{{< /code >}}

### Explaining docker-compose.yaml
Let's talk about each line of docker-compose.  
This is version of the compose
```bash
version: "3.3"
```

Services describes services, another alternative would be volumes
```bash
services:
```

db is a name, could have been bananas
```bash
  db:
```

This is postgres image definition. Image is the image, 11 is a tag. Tags can be found https://hub.docker.com/_/postgres
Ports are defined as external:internal. Internal port is always 5432 for postgresql.
If you have multiple postgresql images, it makes sense to change external (left) port.
Environment is specific to the image
```bash
    image: postgres:11
    ports:
      - 5632:5432
    environment:
      - POSTGRES_USER=root
      - POSTGRES_PASSWORD=root
```

## Dockerfile
We will use Dockerfile to run application on the server.

{{< code lang="docker" >}} 
FROM openjdk:11-jdk-slim

ENV LANG C.UTF-8

ADD build/libs/java-heroes.jar java-heroes.jar

ENTRYPOINT ["java", "-jar", "java-heroes.jar"]

EXPOSE 8000
{{< / code >}} 

Build docker image
{{< code lang="docker" >}} 
docker build . --tag=heroes
{{< / code >}}
tag can be defined as anything

Run docker image
{{< code lang="docker" >}} 
docker run heroes
{{< / code >}}
heroes is the name of the tag defined earlier

NB!! You won't access docker image on localhost.
For linux you can run
```bash
docker ps
docker inspect <container id> | grep "IPAddress"
```
And then go to browser http://<ip>:port

### Explaining dockerfile
Let's talk about each line of dockerfile.  
This defines our base image. On the most primitive level you can define it as ubuntu:xyz https://hub.docker.com/_/ubuntu.
However this is avoided, because images like openjdk are smaller and simpler.
```bash
FROM openjdk:11-jdk-slim
```
Sets language to UTF-8.
```bash
ENV LANG C.UTF-8
```
Following line adds local file *build/libs/java-heroes.jar* to the container with a name **java-heroes.jar**.
Left part must exist (for gitlab CI/CD gradle will build jar first). You can rename right part any way you want.

```bash
ADD build/libs/java-heroes.jar java-heroes.jar
```

This will define what will run when you run ```docker run <tagname>``` 
```bash
ENTRYPOINT ["java", "-jar", "java-heroes.jar"]
```

This translates to 
```bash
java -jar java-heroes.jar```
```
If you want to add more things you can modify ENTRYPOINT.
However this can be limiting and the whole entrypoint can be overridden. (Here overriden with bash)
```bash
docker run <tagname> --entrypoint=/bin/bash java java-heroes.jar
```

This line exposes the port of image. Exposed port should match the port your application is running at.
```bash
EXPOSE 8000
```
