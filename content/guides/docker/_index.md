+++
title = "docker"
description = ""
weight = 10
+++


These guides will help you to install:
1. docker
1. docker-compose

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.
Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. All containers are run by a single operating system kernel and therefore use fewer resources than virtual machines.

Dockerization is ongoing trend therefore important for us to learn as well.

Read more:
1. https://en.wikipedia.org/wiki/Docker_(software)
1. https://www.docker.com/why-docker

Installation is OS dependent. 
It is generally easier and faster to develop using unix systems (mac, linux). 
However good code has been written using windows as well.

{{< childpages >}}
