+++
title = "Mac docker"
description = ""
weight = 1
+++


## docker and docker-compose setup

<img src="../docker.png" height="120">
<img src="../docker-compose.png" height="120">

### There are 2 ways to install docker
1) new docker that has some bugs and doesn't work with Oracle Virtualbox
2) older docker that does work with Oracle Virtualbox

### newer docker setup
Install docker desktop   
https://docs.docker.com/v17.12/docker-for-mac/install/

### older docker setup
Install docker toolbox, it will install docker and docker-compose  
https://docs.docker.com/toolbox/overview/
