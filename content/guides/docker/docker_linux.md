+++
title = "Linux docker"
description = ""
weight = 1
+++


## docker and docker-compose setup

<img src="../docker.png" height="120">
<img src="../docker-compose.png" height="120">

### docker
Ubuntu  
https://docs.docker.com/v17.12/install/linux/docker-ce/ubuntu/  
https://linuxize.com/post/how-to-install-and-use-docker-on-ubuntu-18-04/

Debian  
https://docs.docker.com/v17.12/install/linux/docker-ce/debian/

CentOS  
https://docs.docker.com/v17.12/install/linux/docker-ce/centos/

Fedora  
https://docs.docker.com/v17.12/install/linux/docker-ce/fedora/

### docker-compose
Linux part of 
https://docs.docker.com/compose/install/

### docker daemon issues

Either always run all docker with sudo (easier) 
```bash
sudo docker-compose up
```
or create docker usergroup  
https://docs.docker.com/v17.12/install/linux/linux-postinstall/
