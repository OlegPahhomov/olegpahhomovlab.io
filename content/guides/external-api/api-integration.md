+++
title = "External API integration"
description = ""
weight = 15
+++

### Java ecosystem client

1. Spring RestTemplate (recommended)
    1. https://www.baeldung.com/rest-template
    1. Synchronous http client
    1. Easiest to use
1. Spring WebClient
    1. https://www.baeldung.com/spring-5-webclient
    1. Asynchronous and Synchronous http client
    1. https://stackoverflow.com/questions/47974757/webclient-vs-resttemplate?rq=1
1. Others
    1. Java HttpClient
    1. Feign (netflix) - Creates interfaces to hide api behind it
        - https://github.com/OpenFeign/feign
        - Has Spring native support as well https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
    1. Apache HttpClient
    1. Jetty  
    1. OpenAPI + guardrail - Gives possibility to generate java clients and servers from api specifications.
        - https://www.openapis.org/
        - https://guardrail.dev/
    

Search Google for
```bash
    [my client] baeldung example
```

### How to do it exactly?

It all depends on external API documentation.  
Some require request parameter which you add to your url. 
```&apiKey=my_api_key```

For some you need to add a header. This depends on the client you are using.  
Search Google for  
```bash
   [my client] header example
```

Keep your api keys and secrets private, they should not be public. This is a reason why we use back-end to connect to External API.
These are often kept in configuration files (enough for this subject). 
In real life passwords are overridden on start-up or kept hashed. 
