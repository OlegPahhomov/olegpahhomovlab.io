+++
title = "external-api"
description = ""
weight = 15
+++

An application programming interface (API) is a computing interface which defines interactions between multiple software intermediaries. 
It defines the kinds of calls or requests that can be made, how to make them, the data formats that should be used, the conventions to follow, etc. 

When we build our own API, then other APIs that are connected to our API are referred to as external APIs.

These are just some examples, internet is wide. There must be more APIs available.

##  Weather

<i class="fas fa-snowflake fa-4x text-primary"></i>
<i class="fas fa-cloud-sun-rain fa-4x text-primary"></i>
<i class="fas fa-meteor fa-4x text-primary"></i>

Some ideas:
1. Weather forecast
1. Where to travel? (Sunny place nearby)
1. Compare weather
1. Morale booster (show worse weather elsewhere)
1. etc

### OpenWeather

https://openweathermap.org/api  
It's for free (tier pricing). Register to get an API key.
Examples will be sent to your email.

**NB!** API key takes 1-2h to activate

Current weather example: 
-  ```https://openweathermap.org/current```  
-  ```https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid={your_api_key}```

## Finance
<i class="fas fa-piggy-bank fa-4x text-primary"></i>
<i class="fas fa-landmark fa-4x text-primary"></i>
<i class="fas fa-chart-line fa-4x text-primary"></i>

Some ideas:
1. Portfolio tracking
1. Watchlist
1. Investment opportunities
1. Forex
1. Compare stocks
1. Morale booster (show worse performing stock)
1. etc

### Alpha Vantage

https://www.alphavantage.co/documentation/  
It's for free. Register to get an API key. 

**NB!** Write down the key as they will not send it to you.

Time series daily example  
- ```https://www.alphavantage.co/documentation/#daily```
- ```https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=IBM&apikey=demo```

## Sport
<i class="fas fa-dumbbell fa-4x text-primary"></i>
<i class="far fa-futbol fa-4x text-primary"></i>
<i class="fas fa-quidditch fa-4x text-primary"></i>

Some ideas:
1. League tables
1. Player info
1. Next games
1. etc, etc

### Football-data.org

https://www.football-data.org/documentation/quickstart  
It's for free (tier pricing). Register to get an API key.  
Examples will be sent to your email.

https://www.football-data.org/documentation/quickstart/  
**NB!** For testing by hand will need to use Postman or sth similar  
Add header X-Auth-Token = API_KEY

Real Madrid upcoming matches example:
- ```https://api.football-data.org/v2/teams/86/matches?status=SCHEDULED```

### Open dota API

https://docs.opendota.com
It's for free api to fetch information about Dota2 matches and results. 

Also most of e-sport stuff has great apis to use.

## Music
<i class="fas fa-music fa-4x text-primary"></i>
<i class="fas fa-volume-up fa-4x text-primary"></i>
<i class="fas fa-headphones fa-4x text-primary"></i>

Some ideas:
1. Playlist
1. Album search
1. Musician-song quiz
1. etc, etc

### Last.fm

https://www.last.fm/api/account/create
It's for free. Register to get an API key. 

**NB!** Write down the key and secret.  

Album search example
- ```https://www.last.fm/api/show/album.search```
- ```http://ws.audioscrobbler.com/2.0/?method=album.search&album=believe&api_key={your_api_key}&format=json```

### Spotify
{{< alert style="warning" >}} As it requires OAuth login, it's quite complex {{< /alert >}}

https://developer.spotify.com/documentation/web-api/

You have to be logged in to make every request.
It requires OAuth token so it is a bit complicated.  
https://developer.spotify.com/documentation/web-api/  
https://developer.spotify.com/documentation/general/guides/authorization-guide/  

## RapidAPI and other ideas

RapidAPI has a large library of APIs https://rapidapi.com/  
Be warned most of the APIs cost something.  
Even "free" RapidAPI is free only for first 100-500 requests and then you pay per request. It's a business after all.  

https://docs.rapidapi.com/docs/api-pricing 


{{< childpages >}}
