+++
title = "Project git"
description = ""
weight = 20
+++


For a team project it makes sense to share code between different users. Giving users appropriate rights. 
Compared to [class git](../class-git) project git is simple.

Creating team repository
1) **One** of team members needs to create a private repository for your team using TalTech gitlab.
2) Create repository without a readme file (if you created with readme, just create a new repository)
   1) If you have existing code use ```Push an existing folder``` guide
3) Add your team mates as maintainers or owners
   1) Project information -> Members
4) Add lecturer and mentors as reporters
