+++
title = "master -> main"
description = ""
weight = 100
+++

## Remove racially-charged language from software  

Fueled by black lives matter and other protests software sector is working on removing dependence of racially-charged language.

Words like master/slave, blacklist/whitelist carry unnecessary references to historic injustice.
Instead use neutral words like main/secondary, primary/replica, allowedlist/blockedlist.

Read more
1) https://www.zdnet.com/article/github-to-replace-master-with-alternative-term-to-avoid-slavery-references/
1) https://help.apple.com/applestyleguide/#/apsg72b28652 (search for master)


### Renaming master to main
{{< code lang="bash" >}}
git branch -m master main
{{< /code >}}
_-m means move. That way history is retained_

{{< code lang="bash" >}}
git push -u origin main
{{< /code >}}

Next go inside gitlab repository.  
1. Settings -> Repository -> Default branch -> Update it to be main
1. Settings -> Repository -> Protected branches -> Unprotect master (protect main)  
In big projects it is not allowed to commit directly to main branch.

{{< code lang="bash" >}}
git push origin -d master
{{< /code >}}
