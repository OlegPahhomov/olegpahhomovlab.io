+++
title = "git"
description = ""
weight = 4
+++

<i class="fab fa-git fa-2x text-primary"></i> <i class="fab fa-git-alt fa-2x text-primary"></i> 
is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows

I cannot imagine software development without git.

Most popular git providers are:
1) <i class="fab fa-github fa-2x text-primary"></i> github owned by Microsoft
1) <i class="fab fa-gitlab fa-2x text-primary"></i> gitlab owned by GitLab Inc.
1) <i class="fab fa-bitbucket fa-2x text-primary"></i> bitbucket owned by Atlassian

Read more about git: 
1) https://en.wikipedia.org/wiki/Git
1) https://www.atlassian.com/git/tutorials/what-is-git
1) https://blogs.microsoft.com/blog/2018/06/04/microsoft-github-empowering-developers/

______________
{{< childpages >}}
