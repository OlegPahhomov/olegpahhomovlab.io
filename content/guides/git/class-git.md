+++
title = "Classroom git"
description = ""
weight = 10
+++

Guide to setup git for **classroom coding**. 1 repository, 2 remotes.  
Don't use this for [project](project-git) as this is unnecessarily complex for that purpose.  
This setup is used when modifying an existing library you use, but do not own.  

### Prerequisites
1) git

### Why 
1) you get teachers code
1) you get teachers code updates
1) you get to push and keep your own code

### Visual
<!-- ../ is needed because it renders it as subfolder -->
![](../class-git.png)

### Guide
1) Guide is done once per repository
2) Go to teacher's repository and click **fork**
    1) This creates you a copy of teacher's repository
3) Clone your fork. 
    1) Clone **your copy** repository, not the original teacher's one.
    1) Part 1 done, you got the code. 
    1) However, soon you will realise that it is not up to date. 
    1) Rest of the guide setups the up-to-dateness
4) Open cloned project in IntelliJ
5) Open terminal (IntelliJ, git bash or cmd) in project root 
6) Add lecturer repository as remote named upstream
    ```bash
    git remote add upstream <teacher-repository-url>
    ```
    Following are https and ssh examples for ics0014-2020-java-introduction
    1) https 
        {{< alert style="warning" >}} don't copy this 1:1 {{< /alert >}}
        ```bash
        git remote add upstream https://gitlab.cs.ttu.ee/olpahh/ics0014-2020-java-introduction
        ```
    2) ssh
        {{< alert style="warning" >}} don't copy this 1:1 {{< /alert >}}
        ```bash
        git remote add upstream git@gitlab.cs.ttu.ee:olpahh/ics0014-2020-java-introduction.git
        ```
8) Check that you have 4 remotes (2 fetch, 2 push)
    ```bash
    git remote -v
    ```
9) Fetch upstream to check it works:
    ```bash
    git fetch upstream
    ```
    If it doesn't - you probably made a typo. Remove using command below and add again. 
    ```bash
    git remote rm upstream
    ```

### IntelliJ and git
1) IntelliJ displays active git branch on the bottom right corner.
   1) If you click there you can see all branches
2) IntelliJ has a Git tab on the bottom of the screen.
   1) Git tab has a Log tab. There you can see all the commits.
3) IntelliJ uses colors
   1) Red - uncommited files
   2) Green - new commited files
   3) Blue - changed files
   4) Dark Yellow - git ignored files
4) IntelliJ git commands are inspired from Estonian
   1) Pull is Ctrl + T from _Tõmbama_
   2) Commit is Ctrl + K from _Kommitima_
      1) Push is Ctrl + Shift + K

### Working with multiple remotes
1) First of all there are local branches (branches on your computer) and remote branches
2) Of remote branches
   1) All origin/* are branches from your repository 
   2) All upstream/* are lecturer branches from lecturer repository
3) You can pull from origin and push to origin
4) You can pull from upstream but can't push to upstream
5) It makes sense to have same names for branches locally and remotely. 
6) You have 3 options how to get code from upstream to your code.
    * merge (mix one with another)
    * reset (clear everything, loose local changes)
    * rebase (push upstream commits inside your commits)
7) First you need to fetch upstead
    ```bash
    git fetch upstream
    ```
8) merge, for example main
    ```bash
    git merge upstream/main
    ```
9) reset your branch, for example main 
    ```bash
    git reset --hard upstream/main
    ```
10) rebase, for example main
     ```bash
     git rebase upstream/main
     ```
11) if you get something like this:
CONFLICT (content): Merge conflict in src/main/java/ee/taltech/itcolledge/FirstContact.java
Automatic merge failed; fix conflicts and then commit the result.  
**Then go VCS (up top menu) -> git -> resolve conflicts**
12) merge, rebase and reset commands can be used from Intellij.  
Merge and rebase are accessible by right clicking in branches menu. 
Reset is a bit more complex, under git log you see commit history, 
there you can click reset current branch to here and pick hard option

### Working with class git at home
1) If you pull the code at home you will have all the changes
   1) Your code will have all assignments completed, all problems solved
   2) Not good, right!?
2) First thing is to make sure you start with the same commit as the lecturer
   1) So notice what commit does lecturer start on.
   2) Go to Git -> Log, find the commit
   3) (ITI0302 iti0302-2021-heroes-back: As of writing this for lecture 2 lecturer is on commit 'Update package 04.09.21')
   4) (ICS0024 ics0024-2021-intro-code: As of writing this for lecture 2 lecturer is on commit 'Init class assignments 18.07.21' )
   5) Right click -> Reset current branch to here or some other way
3) Lecturer commits and pushes every change
   1) Either cherry-pick that change or compare changes
      1) If it's something more complex like adding db then cherry-pick as you might make an error
   2) Cherry-pick: right click new commit -> Cherry-Pick
      1) It is easier if you don't cherry-pick out of order
   3) Compare changes: right click new commit -> Compare with Local
