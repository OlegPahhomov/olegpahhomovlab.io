+++
title = "Guides"
description = ""
weight = 1000
+++

This is collection of all how to install guides.

### Quicklinks
{{< childpages >}}

### Guides & courses
{{< table style="table-striped" >}}
| guide                            | ics0014 2021                 |    ics0024 2020              |    iti0203 2020  |
| -------------                    | -------------                | -------------                | -----------      |
| [git](git)                       | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| [back-end](back-end)             | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| [front-end](front-end)           | <i class="fas fa-times"></i> | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> |
| [docker](docker)                 | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| [production](production)         | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
| [external-api](external-api)     | <i class="fas fa-times"></i> | <i class="fas fa-check"></i> | <i class="fas fa-check"></i> |
{{< /table >}}

