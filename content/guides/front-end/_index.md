+++
title = "front-end"
description = ""
weight = 5
+++

{{< panel title="A poem" style="secondary" >}} 
Unknown creates discomfort,   
discomfort creates dislike,   
dislike creates fear   
and fear creates back-end developers.
 {{< /panel >}}

This guide will help you to install Angular front-end.

### More on es6 and angular
1) ES6 examples https://www.freecodecamp.org/news/write-less-do-more-with-javascript-es6-5fd4a8e50ee2/
1) ES6 examples https://itnext.io/why-you-should-use-es6-56bd12f7ae09
1) Style guide https://angular.io/guide/styleguide
1) Another official sample app https://angular.io/start
1) Todo app https://www.sitepoint.com/angular-2-tutorial/

### Angular vs react vs vue
Comprehensive comparison in 2020
1) https://www.codeinwp.com/blog/angular-vs-vue-vs-react/

### Alternatives to angular
In the class we are using angular.
If you want to use react or vue, please note that you must be able to develop proper 
authentication/authorization management (part 3).  

Search google for 
```bash
[my framework] authentication jwt
```

Some quick searches:
* react: https://jasonwatmore.com/post/2019/04/06/react-jwt-authentication-tutorial-example  
* vue: https://bezkoder.com/jwt-vue-vuex-authentication/

{{< childpages >}}
