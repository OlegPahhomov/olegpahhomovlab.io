+++
title = "Angular Material"
description = ""
weight = 1
+++


## Angular material install guide for angular based projects

https://material.angular.io/

1. Install the library
    ```bash
    ng add @angular/material
    ```
    1. Pick a theme you like, say yes to other options
1.  Result of installation is not ideal for modern use. Nowadays angular-material components are extracted into seperate module.  
    1.  Create a new module. angular-material.module.ts
        ```js
        import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
        import {NgModule} from '@angular/core';
        
        @NgModule({
          exports: [
            BrowserAnimationsModule
          ],
        })
        export class AngularMaterialModule {
        }
        ```
    1.  Update app.module.ts to import AngularMaterialModule instead of BrowserAnimationsModule
        ```js
        import {AngularMaterialModule} from './angular-material.module';

        @NgModule({
            ..    
            imports: [
                ..,    
                AngularMaterialModule
            ],
            ..
        })
        ```
1. Install flex-layout for angular-material
    ```bash
   npm install @angular/flex-layout --save 
   ```
1. fxLayout guide https://github.com/angular/flex-layout


Angular-material search heroes example from the internet:  
1. [article link](https://itnext.io/using-angular-6-material-auto-complete-with-async-data-6d89501c4b79)
1. [code link](https://stackblitz.com/edit/angular-material-autocomplete-async2)
