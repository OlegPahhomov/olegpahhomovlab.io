+++
title = "Angular"
description = ""
weight = 1
+++


## Front-end installation guide for angular heroes

### Global dependencies

1) install node  https://nodejs.org/en/ 
   1) Choose LTS (long term support) 
   2) node installs npm
   3) ```node -v``` to check the version, try to have v14.x
2) install angular cli (command line interface)
   ```bash
   npm install -g @angular/cli
   ```
   1) angular-cli installs ng command
   
**NB!!** IntelliJ terminal might not recognize npm and ng commands.   Either IntelliJ or your computer should be restarted.

### Running existing project
1) Fork & clone lecturer front-end repository using [git guide](../../git/class-git)
2) to install dependencies run in the project root
   using npm
   ```bash
   npm install
   ```
   if npm complains and offers to run it with --force
   ```bash
   npm install --force
   ```
4) to start local web server run in the project root  
   ```bash
   ng serve --open
   ```

### Creating a new project
1) navigate to root of the project
1) use angular cli to init project in the existing directory
    ```bash
    ng new angular-heroes --directory ./
    ```
2) install dependencies
   using npm
   ```bash
   npm install
   ```
3) to start local web server run  
   ```bash
   ng serve
   ```
   **or**
   ```bash
   npm run start
   ```
### build
Build commands will be useful for production deploy.
1) build project
   ```bash
   npm run build
   ``` 
1) build project for production
    ```bash
    yarn build --prod
    ```

### dependencies
Dependency is a dependency used for production, for example style or js library like moment or lodash.  
DEV dependency is a dependency used for development purposes. Some sort of compiler plugins or text editors.  
1) Adding dependencies:
    ```bash
    npm add <dependency-name>
    ```
1) Adding DEV dependencies:
    ```bash
    npm add --dev <dependency-name>
    ```

### npm vs yarn
1) install yarn (optional) yarn is alternative to npm
    ```bash
    npm install yarn -g
    ```
{{< table style="STYLE" >}}
| what        | npm           | yarn  |
| ------------- |-------------| -----|
| install dependencies  | ```npm install``` | ```yarn``` |
| run dev server     | ```npm run start```  | ```yarn start``` |
| build              | ```npm run build```      |   ```yarn build``` |
| build prod         | ```npm run build --prod```      |   ```yarn build --prod``` |
| add dependency     | ```npm add <dependency-name>```         |    ```yarn add <dependency-name>``` |
| add dev dependency | ```npm add --save-dev <dependency-name>```  |    ```yarn add --dev <dependency-name>``` |
{{< /table >}}
npm run *scriptname*, you can define your scripts differently altering their functionality
