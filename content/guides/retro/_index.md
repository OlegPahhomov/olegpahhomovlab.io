+++
title = "Retro"
description = ""
weight = 10000
+++

Scrum development is all about getting the team better.  
In our classes we perform retrospectives.
The goal is to improve team performance.

## Homework - Retro
Submit in team chat a Google doc (or alternative link) with following content:
https://www.scrum.org/resources/what-is-a-sprint-retrospective
* What worked well?
* What went bad? / What could be improved?
* What will we commit to improve in the next Sprint?

You can make the retro fun by using: https://funretro.io/

## Homework - 2nd/3rd Retro (+1 question)
Submit in team chat a Google doc (or alternative link) with following content:
https://www.scrum.org/resources/what-is-a-sprint-retrospective
* What worked well?
* What went bad? / What could be improved?
* What will we commit to improve in the next Sprint?
* **new** Compare retro 2 vs retro 1. What is the progress?

You can make the retro fun by using: https://funretro.io/

{{< childpages >}}
