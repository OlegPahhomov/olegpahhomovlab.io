+++
title = "Virtual memory"
description = ""
weight = 1
+++

## Adding virtual memory

Free tier EC2 instance has only 1Gb of RAM. Front-end builds run out of memory. 

The solution is to turn on / increase virtual memory size.
By default virtual memory is turned off.

To see virtual memory: 
* linux run ```htop```, 
* windows https://www.geeksinphoenix.com/blog/post/2016/05/10/how-to-manage-windows-10-virtual-memory.aspx
    * this is for your information only

### htop
htop is linux alternative to task manager. Install it on EC2
```bash
sudo apt-get install htop
```

### Add virtual memory

Log into EC2.

Follow the guide:
https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-16-04
