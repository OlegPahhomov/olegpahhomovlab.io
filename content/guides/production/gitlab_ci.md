+++
title = "Gitlab CI"
description = ""
weight = 1
+++

## gitlab CI

This is example of .gitlab-ci.yml  
Successful build creates pipeline in your repository.
Repository -> CI/CD -> Pipelines

Detailed reference: https://docs.gitlab.com/ee/ci/yaml/

### backend example
{{< code lang="yaml" >}} 
stages:
  - build
  - test
  - deploy

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

build heroes:
  stage: build
  cache:
    paths:
      - .gradle/wrapper
      - .gradle/caches
  artifacts:
    paths:
      - build/libs
  tags:
    - heroes
  script:
    - ./gradlew assemble

test heroes:
  stage: test
  tags:
    - heroes
  script:
    - ./gradlew check

deploy heroes:
  stage: deploy
  only:
    refs:
      - master
  tags:
    - heroes
  script:
    - mkdir -p ~/api-deployment # mkdir make folder api-deployment ~/ is under current user directory so for gitlab it would be /home/gitlab/api-deployment
    - rm -rf ~/api-deployment/* # rm remove -rf is recursive files from api-deployment
    - cp -r build/libs/. ~/api-deployment # cp - copy build/libs is where
    - sudo service heroes restart  # this requires sudo rights for gitlab user
{{< / code >}} 

### frontend example
{{< code lang="yaml" >}} 
stages:
  - build
  - deploy

build heroes:
  stage: build
  image: node:12-alpine
  cache:
    paths:
      - node_modules
  artifacts:
    paths:
      - dist
  tags:
    - heroes
  script:
    - npm install
    - npm run build

deploy heroes:
  stage: deploy
  tags:
    - heroes
  script:
    - mkdir -p ~/front-deployment
    - rm -rf ~/front-deployment/*
    - cp -r dist/angular-heroes/. ~/front-deployment
{{< / code >}} 

### Explaining .gitlab-ci.yml

Stages are blocks of the build, you can define any number of stages.
```yaml
stages:
  - build
  - test
  - deploy
```

Following defines a block which is run during stage: test. Tags are connected to gitlab runners. You might want some builds to run on different runners.
```yaml
test heroes:
  stage: test
  tags:
    - heroes
  script:
    - ./gradlew check
```

```assemble, check``` are gradle builds https://docs.gradle.org/current/userguide/java_plugin.html

Because gitlab-runner is installed on the same machine as production we can just copy (override) and restart application.
```yaml
deploy heroes:
  stage: deploy
  only:
    refs:
      - master
  tags:
    - heroes
  script:
    - mkdir -p ~/api-deployment
    - rm -rf ~/api-deployment/*
    - cp -r build/libs/. ~/api-deployment
    - sudo service heroes restart
```
