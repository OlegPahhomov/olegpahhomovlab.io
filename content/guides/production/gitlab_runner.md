+++
title = "Gitlab runner"
description = ""
weight = 1
+++

## Installing gitlab runner

Gitlab CI needs a runner to run CI configuration. 
Problem is public runners are very slow. 
Therefore it is easiest to configure custom runner.

https://docs.gitlab.com/ee/ci/quick_start/

1. EC2. Follow https://docs.gitlab.com/runner/install/linux-manually.html
    * We have Linux x86-64
    * We don't need to configure docker
    
1. Gitlab. Settings -> CI/CD -> Runners
    * You want url and token
    * Next step will ask for them
    
1. EC2. https://docs.gitlab.com/runner/register/index.html 
    * enter url and token from Gitlab
    * executor is shell
        * to change it edit ```/etc/gitlab-runner/config.toml```

1. Configuring CI in gitlab 
    * .gitlab-ci.yml in project root
    * Official guide https://docs.gitlab.com/ee/ci/yaml/
    * Format is yaml https://yaml.org/
    
1. You will have to repeat this process for front-end repository
    * If you are using 1 machine for everything (99% of you) you only need to configure it (it is already installed) 
    * Frontend and backend have different builds
    * Frontend and backend gitlab tokens are different
    * Runner can execute front and back builds at the same time
