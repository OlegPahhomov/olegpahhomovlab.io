+++
title = "AWS EC2 server"
description = ""
weight = 1
+++

## AWS account setup

AWS stands for Amazon Web Services.  
AWS offers some services for free for 12 months. 
https://aws.amazon.com/free

Go to https://aws.amazon.com and register
* Region is North-Europe or sth else close to Estonia

### Creating EC2 instance

EC2 is a simple virtual machine provided by AWS.
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html


1) Go to AWS management console
1) Find EC2
1) Left menu Instances
1) Launch Instance
1) Select Ubuntu Server 20.04 LTS (HVM), SSD Volume Type
    * It is easier to get application running on linux than on windows
    * Ubuntu offers better Docker support than Amazon linux
1) Pick t3.micro (free tier eligible)
1) Click next till you reach storage. Set storage to 16-30Gb.
    * Extra storage will enable to turn on virtual memory
1) Click next till you reach security groups. Add another security group. 
    * Click Add. Select All traffic and source is 0.0.0.0/0, ::/0
        * You can update it later: https://stackoverflow.com/questions/26818456/amazon-ec2-not-working-when-accessing-through-public-ip
    * This will allow rest of the world to access our front-end application
    * Don't remove ssh security group
1) Click Launch and review. Then Launch.
1) It will prompt you for public/private keys.
    * Create new and save it
    * If you delete or loose the private key you won't be able to access your EC2 instance.
1) Launch instances
