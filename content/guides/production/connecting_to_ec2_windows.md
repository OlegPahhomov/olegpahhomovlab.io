+++
title = "Connecting to EC2 instance (Windows)"
description = ""
weight = 1
+++

## Connecting to EC2 instance

Dealing with servers is easier on unix systems (mac, linux).
If you have dual boot, I highly recommend switching from windows.
On windows you can't use build automation tools like Ansible.

### Connecting using putty
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html

### Notes
Once you stop an instance it gets a new public IP. 
