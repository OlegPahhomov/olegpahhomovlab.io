+++
title = "Connecting to EC2 instance (Linux, mac)"
description = ""
weight = 1
+++

## Connecting to EC2 instance

Dealing with servers is easier on unix systems (mac, linux). Congrats.
On unix you can use build automation tools like Ansible.

### ssh connection

1) Go to AWS management console
1) Left menu Instances
1) Connect
1) You will need Public DNS

I recommend storing private key (.pem) in ~/.ssh folder
If you don't have ssh. Install it.

```bash
ssh -i private_key.pem ubuntu@aws-public-ip
ssh -i ~/.ssh/private_key.pem ubuntu@aws-public-ip
```

##### Speed it up
1) Create a scripts folder. My 2 favorite locations.
    * <project_root>/conf (git ignore the folder)
    * <project_root>/../scripts
1) Create file "ssh server-name"
1) Copy to new file. Update the references.
     ``bash
    #!/bin/bash
    
    cd "${0%/*}"; if [ "$1" != "T" ]; then gnome-terminal -e "'$0' T"; exit; fi;
    
    ssh -i ~/.ssh/private_key.pem ubuntu@aws-public-ip
    
    read ENTER
    ``
    Following line opens gnome terminal in new window.
    Mac users should use similar command to open mac terminal in new window.
    ```bash
    cd "${0%/*}"; if [ "$1" != "T" ]; then gnome-terminal -e "'$0' T"; exit; fi;
    ```
1) Allow file to execute as a program
    ```bash
    chmod +x filename
    ```
    or right click properties, permissions and tick [x] allow executing as a program

### Notes
Once you stop an instance it gets a new public IP.
