+++
title = "back-end"
description = ""
weight = 5
+++

These guides will help you to install:
1. git
1. JDK (java development kit)
1. IntelliJ

Installation is OS dependent. 
It is generally easier and faster to develop using unix systems (mac, linux). 
However good code has been written using windows as well.

{{< childpages >}}
