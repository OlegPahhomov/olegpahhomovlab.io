+++
title = "Mac back-end"
description = ""
weight = 1
+++

## install brew for git

Install brew (https://brew.sh/)
{{< code lang="bash" >}}
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
{{< /code >}}
Git will be installed automatically with brew

## install java jdk 11

For ios AdoptOpenJDK is available.
Make sure to use **version 11** as it is LTE (long term support).  
Save yourself some time and do not install other versions.

{{< code lang="bash" >}}
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk11
{{< /code >}}

## install IntelliJ

IntelliJ IDEA is nr 1 java development environment. Registering with TalTech email will guarantee a licence for Ultimate.

1) Download it from https://www.jetbrains.com/idea/download
1) Register with TalTech email and you will get free student licence.
1) To login in IntelliJ click from top menu help -> register
1) Install nice [plugins](../intellij-plugins)

### cloning project in IntelliJ

Following is an example for github. So replace github with gitlab.  
In the class we are defining [class git](/guides/git/class-git) so make sure you are cloning the right project.

{{< youtube aBVOAnygcZw >}}
