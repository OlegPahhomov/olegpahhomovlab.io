+++
title = "IntelliJ plugins"
description = ""
weight = 5
+++

Open IntelliJ and then navigate to File -> Settings -> Plugins
1. key promoter X 
    1. it tells you to use shortcuts
    1. https://plugins.jetbrains.com/plugin/9792-key-promoter-x
1. sonarlint
    1. it tells you how to write code
    1. https://plugins.jetbrains.com/plugin/7973-sonarlint/
1. lombok plugin (if you don't have it)
    1. it enables annotations for getters, setters and other useful things
    1. https://plugins.jetbrains.com/plugin/6317-lombok
1. presentation assistant 
    1. it shows you the shortcuts you use on bottom of the screen
    1. https://plugins.jetbrains.com/plugin/7345-presentation-assistant

