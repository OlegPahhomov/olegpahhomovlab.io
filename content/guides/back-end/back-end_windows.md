+++
title = "Windows back-end"
description = ""
weight = 1
+++

## install or update git

Download it from https://git-scm.com/download/win  
Install or update it. It is a good idea to have the latest version of git.

## install java jdk 11

You can install AdoptOpenJDK or OracleJDK. Oracle requires an account, so Adopt is probably easier.
Make sure to use **version 11** as it is LTE (long term support). Save yourself some time and do not install other versions.

### AdoptOpenJDK
Download and install https://adoptopenjdk.net/
_make sure it's 11_

### Oracle JDK
Download and install (you will have to create an account)  
https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html
_make sure it's 11_

### Others
Or if you really know what you're doing here is compilation of all jdk's.  
https://www.azul.com/downloads/zulu-community/?architecture=x86-64-bit&package=jdk

## install IntelliJ

IntelliJ IDEA is nr 1 java development environment. Registering with TalTech email will guarantee a licence for Ultimate.

1) Download it from https://www.jetbrains.com/idea/download
1) Register with TalTech email and you will get free student licence.
1) To login in IntelliJ click from top menu help -> register
1) Install nice [plugins](../intellij-plugins)

### cloning project in IntelliJ

Following is an example for github. So replace github with gitlab.  
In the class we are defining [class git](/guides/git/class-git) so make sure you are cloning the right project.

{{< youtube aBVOAnygcZw >}}
