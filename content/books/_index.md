+++
title = "Book recommendations"
description = ""
weight = 3000
+++

Easy reading
1. Clean Code: A Handbook of Agile Software Crafts, Robert Cecil Martin
1. The Phoenix Project: A Novel about It, Devops, and Helping Your Business Win, Gene Kim, Kevin
Behr and George Spafford
1. Scrum: The Art of Doing Twice the Work in Half the Time, Jeff Sutherland
1. The Clean Coder: A Code of Conduct for Professional Programmers, Robert C Martin

Programming
1. The Pragmatic Programmer: From Journeyman to Master, Andrew Hunt and David Thomas
1. Test Driven Development: By Example, Kent Beck
1. Experiences of Test Automation: Case Studies of Software Test Automation, Dorothy Graham and
Mark Fewster

Hardcore java
1. Effective Java, Joshua Bloch
