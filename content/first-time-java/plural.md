+++ title = "Part 3 plural"
description = ""
weight = 3
+++

## Plural

Most of the things can be had in plural. Java has both arrays [ ] and lists. 

### lists

Lists are preferred as their offer more convenient use. Lists are declared with interface on the left and implementation on the right side.
ArrayList is used 99.5% of the time.
```bash
List<Integer> numbers = new ArrayList();
```

You can get and add items to the list. You can also remove items, but that is usually avoided. I will come back to it.
```bash
numbers.add(1);
numbers.get(0);
```

Lists are created with a type List\<type\>. It is called generics. Generics help us to enforce type safety. List of Integers will have only Integers in it.
```bash
numbers.add(1);
numbers.add("banana"); //this will not compile
```

With Java 11 it is easy to create and fill a list on creation. This creates an immutable list. Immutable means unchangeable. So you cannot add anything.
```bash
List<Integer> numbers = List.of(1, 2, 3);
```

### iteration

The fun part is how to iterate a list.
Type iter and tab to produce **for-each** loop.
```bash
for (Integer number : numbers) {
            
}
```

There is also ability to stream
```bash
numbers.stream()
    //. map/filter/collect
```

There is also **for-i** loop which is used as a last resource.
Type fori and tab.
```bash
for (int i = 0; i < numbers.size(); i++) {
   Integer number = numbers.get(0);            
}
```

### maps

Once lists get too big, so 0.0001% of the time they are mapped to a Map. Map has a key and value and is faster for element retrieval.
```bash
Map<Integer, Integer> map = new HashMap<>(); 
```

iter and tab works with maps as well
You can iterate over keys, values or both (called entries).  

### filter instead of removing

Ah yes I promised to talk about removing. 
Well if someone says remove all negative numbers from the list it is clearer to create a new list with only positives in it.
Removing elements from the list modifies existing list which can lead to countless errors. So prefer following pattern. 

for-each
```bash
List<Integer> originalList = List.of(-1, 2, 3);
List<Integer> positives = new ArrayList<>();
for (Integer num : originalList) {
    if (num >= 0) {
        positives.add(num);
    }
}
```

stream
```bash
List<Integer> originalList = List.of(-1, 2, 3);
List<Integer> positives = originalList.stream()
  .filter(num -> num >= 0)
  .collect(Collectors.toList())
```

You are ready
