+++ title = "Part 2 static vs instance"
description = ""
weight = 2
+++

## Static vs instance
We used static once in the last example. I sneaked it in. Did you notice?
```bash
public static void main(String[] args) {
  
}
```

Static can be used with methods and fields (also classes but that's outside our scope)

Static is the opposite of an instance. 
We create instances of coke (Coca-Cola 0.33L cans) or coffee (a cup of coffee latte with extra cinnamon).
With static, it is something we do not create instances of as we don't need them. Think ocean or humanity.
We can create an of a class, but it's unnecessary.

<i class="fas fa-gavel"></i>
_Industry standards time! One doesn't mix and match static and instance. For each class you make up your mind what it is.
In the future (with spring) we will introduce a concept of singletons which is a single class instance (sort of acting like static)._  

Static methods are excellent for describing mathematical functions. 
You just say doMatchStuff(inputs). For example summing 2 numbers.
```bash
public static int sum(int x1, int x2) {
  return x1 + x2;
}
```

A class would look like
```java
public class SimpleMathCalculator {
    
    public static int sum(int x1, int x2) {
        return x1 + x2;
    }
}
```

We could call it with. _Inside a magical main method or a test_
```bash
int result = SimpleMathCalculator.sum(3, 10);
System.out.println(result);
System.out.println(SimpleMathCalculator.sum(5, 14)); //inlined version
```

If you didn't make your method static it would look like. 
```bash
SimpleMathCalculator calculator = new SimpleMathCalculator();
int result = calculator.sum(3, 10);
System.out.println(result);
System.out.println(calculator.sum(5, 14)); //inlined version
```
_Each time code block is executed a new calculator instance is created._

<br/>

<i class="fas fa-gavel"></i>
_Industry standards time! Classes with static methods usually have their constructors hidden. This can be done by private Constructor._
```java
public class SimpleMathCalculator {
    
    private SimpleMathCalculator() {
        
    }
    
    public static int sum(int x1, int x2) {
        return x1 + x2;
    }
}
```

### Takeaways

1) Instances (no static) are used when we want to create instances of our class 
1) Static is used when we do not want instances like utility functions
1) Don't mix and match.
