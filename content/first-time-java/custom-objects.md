+++ title = "Part 4 custom objects"
description = ""
weight = 4
+++

Sometimes Java types are not enough.
Let's say you want to return 2 lists.

What to do?

Java is object-oriented programming, 
therefore we create a new object to wrap the properties we would like to return.


```java
public class ListsResponse {
    private List<Integer> list1;
    private List<Integer> list2;
    
    //getters and setters omitted for brevity
}
```

In Python <i class="fab fa-python"></i>, I could have used a tuple! 
Let's consider readability

```java
public class Example {
 
    //option 1
    public Pair<List<Integer>, List<Integer>> toOddsAndEvens(List<Integer> input) {
        //logic omitted for brevity
    }

    //option 2
    public ListsResponse toOddsAndEvens(List<Integer> input) {
        //logic omitted for brevity
    }
}
```

2nd option is better to read. It could be even better if response class name was better.
