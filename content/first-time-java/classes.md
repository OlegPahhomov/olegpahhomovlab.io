+++ title = "Part 1 dipping toes in Java"
description = ""
weight = 1 
+++

## Here you will learn

1) Abstraction
1) OOP
1) Class vs object

<br/>

Java is object-oriented language. 
```bash
teacherOleg.sendMessage(students, "That means everything coded in Java happens through objects"). 
```

## Abstraction
First let's talk about **abstraction**.
The objects we use in the code are abstractions - simplifications where everything unnecessary is removed.  

When we write
```bash
Person person = new Person();
```
then this person is not real but might represent a real person. (For example a human registering to your website)

{{< panel title=" " style="secondary" >}}
Abstraction is the process of removing physical, spatial, or temporal details or attributes in the study of objects or systems 
in order to more closely attend to other details of interest
{{< /panel >}}

## Object oriented programming

{{< panel title=" " style="secondary" >}}
Object-oriented programming (OOP) is a programming paradigm based on the concept of "objects", which may contain data, 
in the form of fields, often known as attributes; and code, in the form of procedures, often known as methods.
{{< /panel >}}

**Practise** 
Think you are explaining how to make coffee to an alien. The alien has never seen coffee machine and has never been to the kitchen.
You have to use word object when referring to every item and if you encounter plural you will have to iterate through the objects. 

## Class vs object
**Class** is a template for an object. A template for a person could be class Person.
```java
public class Person {

}
```
Note that the name of the public class (Person) and file name must match. Otherwise, it doesn't work.
We have same the example in our class code.


Okay, I left few things out. There is a package which is a location of the class in the project. 
And... then there are imports that tell IDE where to get the other code you used (imported). 
```java
package xyz;

import com.library.*;

class Person {

}
```

Okay, I still didn't tell full story. Each class has default constructor which is no-args constructor. It is there "magically" till you create your own.
```java
public class Person {

    public Person() {
        
    }
}
```

So, how do we get object out of a class?

When we write
```bash
Person person = new Person();
```

We are creating a new object of class Person. You are creating an instance of a class.  
You can say _an instance of a class_ or _new class object_. I guess instance of a class sounds better (like string vs text).


## Person 2.0

Okay let's improve. Let's add a person name and print it out.

1) first we will set a name
1) then we will pass a name

If these hints are enough, you can do it on your own and compare. 

### Setting a name
A name could be stored to a field (instance field). It could be String.

<i class="fas fa-gavel"></i> 
_Industry standards time! Instance fields are declared as private with public getters and setters. 
Getters and setters are generated easily by IDE. Google for encapsulation._ 

```java
public class Person {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

Now we need to print it out. Each Java class has a magic method which is used to run the code. 
_In the future we will build an application, and it will have one such method._   
```bash
public static void main(String[] args) {
  
}
```

And to print
```bash
System.out.println("Print me")
```

And to create a Person
```bash
Person person = new Person();
```

Aaaand to put a name inside a Person we will need to use the setter (okay you can just go person.name as you are in the same class)
```bash
person.setName("New name")
```

**Practise** Put it all together. I can wait <i class="fas fa-hourglass-half"></i>

### Solution
Okay this is what I did
```java
public class Person {

    public static void main(String[] args) {
        Person bob = new Person();
        bob.setName("Bob");
        System.out.println(bob.getName());
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```
### Passing a name to a Constructor
We can also pass a name to the Constructor. It's a design choice. It is **your** design choice. There will be more. 

If you add a constructor with a name the magic no-args constructor will disappear and IntelliJ will complain. So here I am adding 2 constructors
```bash
public Person() {
}

public Person(String name) {
    this.name = name;
}
```

It means I can go like this

```bash
Person person = new Person("A name of a person");
Person person2 = new Person(); //no name on creation
```

### Solution
This is the end result

```java
public class Person {

    public static void main(String[] args) {
        Person bob = new Person();
        bob.setName("Bob");
        System.out.println(bob.getName());

        Person mary = new Person("Mary");
        System.out.println(mary.getName());
    }

    private String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

### Formatting
IntelliJ provides you with quick formatting. 
```bash
Code -> Reformat code
```
Learn that shortcut as it is easier than learning how to format.

Java works with correctly placed brackets, mostly ( ) and { }. If you get these right and format afterwards it will look nice.
Java is writtenInCamelCase, only classes start with a big letter ClassesStartBig. 
