+++
title = "ICS0014 2022"
description = ""
weight = 1
+++

## Goal of ICS0014 Java Technologies 2022

In this course:
1) Learn basics of java (latest and greatest)  
2) Build a modern backend API application with Spring Boot

## Introduction
Course is on slack: https://ics0014-2022.slack.com  **To sign up**: https://ics0014-2022.slack.com/join/signup  
Slack is where I answer your questions and communicate with you.  
Slack is used by businesses around the world (although lately Teams has been growing faster).  
I recommend you create a bookmark folder and put everything from our course there (this is how I roll).

Course is 8 weeks long: there is a 3h lesson each week. Theory and practise are taught together.
Few lessons will be on-site, but mostly on-line.  
It is advisable to bring your own laptop, however there are also computers supplied by university.

Course ends with a **grade**. Course is done in teams of 2-4 people.  
To pass you need to complete 1 [project](ics0014-2022-project) which is graded in 2 parts:
1) Java
1) Backend API

## Materials
* [Slides](https://drive.google.com/drive/folders/1qRg2rk39TJi1462AU0SKiFhhrY8i-Qsb?usp=sharing)
  * ``https://drive.google.com/drive/folders/1qRg2rk39TJi1462AU0SKiFhhrY8i-Qsb?usp=sharing``
* [Videos](https://echo360.org.uk/section/1f025061-865e-471a-add6-f7ea31676932/public)
  * ``https://echo360.org.uk/section/1f025061-865e-471a-add6-f7ea31676932/public``
  * **NB!** After you open it the link changes and new link won't work
* [Class code for java](https://gitlab.cs.ttu.ee/olpahh/ics0014-2022-java)
  * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2022-java``
* [Class review code for java - java zero-to-hero](https://github.com/OlegPahhomov/java-zero-to-hero)
  * ``https://github.com/OlegPahhomov/java-zero-to-hero``
* [Class code for spring boot](https://gitlab.cs.ttu.ee/olpahh/ics0014-2022-spring)
  * ``https://gitlab.cs.ttu.ee/olpahh/ics0014-2022-spring``

### Class code for java - git branch logic
**main** - assignments without solutions  
**solutions-2022** - solutions from 2022 classroom (sporadic theme 1-3 solutions)  
**solutions-online-2022** - solutions from 2022 short solutions videos (theme1 only)  
**solutions-online-2021** - solutions from 2021 online recordings (videos for theme 2-3, solutions for all themes)  

### Setup
What to install during the course. Please refer to [ics0014-2022-class#introduction](ics0014-2022-class/#introduction)

## Final grade
Grade is proportion of student’s points to a total number of possible points. Extras contribute to student, but not to total points.
```bash
(p1 + p2) / (p1max + p2max) * 100  
```  
{{< table style="table-striped" >}}
| percentage      | grade        | 
| ------------- |:---------------:| 
| 51+   | pass | 
| 0-50  | fail | 
{{< /table >}}

## Asking for help
Contact lecturer on Slack. Come after the lecture. For group project related questions and problems use team chats.

## Other courses 
This is my last course.
For Autumn, I recommend courses of German Mumma.
1) ICS0024 Automated Testing (he should inherit this one to my knowledge)

I used to do
1) ICS0024 Automated Testing
2) ITI0302 IT project (this might be interesting for web development - front/back/infra) 
