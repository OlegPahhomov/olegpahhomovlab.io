+++
title = "ICS0014 2022 Deadlines"
description = ""
weight = 1
+++
{{< readfile file="/fragments-2022/ics0014/grading-excel.md" markdown="true" >}}


## Checkpoint 1
[Checkpoint 1](../ics0014-2022-project/#project-1-java) is a warm-up for project part 1 - [Java](../ics0014-2022-project/#project-1-java)
{{< table style="table-striped" >}}
| deadline      |            date            | notes                  |
| ------------- |:--------------------------:|------------------------|
| on time | Saturday, **12.02 23:59** | message to submit [link](../ics0014-2022-project/#checkpoint-1) |
{{< /table >}}

## Part 1 - Java
[Java](../ics0014-2022-project/#project-1-java)
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2022/ics0014/deadline-project-1.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

## Homework after part 1
Homework will be given a day after you submit.
{{< table style="table-striped" >}}
| deadline      |            date            | notes                  |
| ------------- |:--------------------------:|------------------------|
| on time | **4 days after part 1** | don't worry it's easy |
{{< /table >}}

## Part 2 - Backend API
[Backend API](../ics0014-2022-project/#project-2-backend-api)  
{{< table style="table-striped" >}}
{{< readfile file="/fragments-2022/ics0014/deadline-project-2.md" markdown="true" >}}
{{< /table >}}
_works after **late deadline** are 0_

### Defence
Online university teams defence after Part 2 is graded. I will send you a place to register.
[Defence info](../ics0014-2022-project/#defence)  
