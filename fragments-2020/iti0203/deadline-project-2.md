| deadline      | date        | notes  |
| ------------- |-------------| -----|
| early bird | Saturday, 07.11.2020 23:59 | extra points |
| on time | Saturday, 14.11.2020 23:59 | each hour late is -2% rounded upwards |
| late | Saturday, 21.11.2020 23:59 | 1 week late -30% |
