* [The grading excel](https://docs.google.com/spreadsheets/d/1xL5ZYcrkItf9GwaQCHkhC63BYNzGQE6FB2qdI2Lqh5Q/edit?usp=sharing)
* Wow, the grading excel is complex!? 
    * Yeah, we are trying to expand your horizon
    * There is git, API standards, business logic, spring, front-end.
* Deduction + extras system
    * Deduction is designed to reduce max points from 100% to 90% (5 -> 4)
    * These points can be regained by doing different extras
    * Extras give you opportunity to explore different topics
* We will grade main/master branch only 
    * [master -> main](../../git/main)
* Each team gets individual feedback
    * points + comments
* Small & beautiful is better than big and messy
* Spring Security is not graded in part 1
