| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | Saturday, 10th October 23:59 | extra points |
| on time | Saturday, 17th October 23:59 | each hour late is -2% rounded upwards |
| late | Saturday, 24th October 23:59 | 1 week late -30% |
