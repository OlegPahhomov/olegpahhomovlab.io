* [The grading excel](https://docs.google.com/spreadsheets/d/1xL5ZYcrkItf9GwaQCHkhC63BYNzGQE6FB2qdI2Lqh5Q/edit?usp=sharing)
* Structure and points are similar to Part 1. Following things are new.
    * We want you to rewrite your overview and documentation to show what was done in part 1 and what will be done in part 3. You can include refactoring and bugs in the planned tasks.
    * User stories are graded for working well. Bug-free. As expected.
    * Spring Security is graded. User can register. Admin/user can access their respective resources.
    * Part 2 still works.
    * Your application looks good in mobile.
