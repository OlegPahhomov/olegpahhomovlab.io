* To defend 2 things must happen: 
    1) you submitting 
    2) us grading
* After that registration doodle will be sent to your team chat
* Defences last 15 minutes and will be done during the first part of the regular lesson
* General defence guidelines
    * During the defence you will show your application (not code)
    * Come 5 minutes early and prepare your environments.
        * Database up
        * Backend up
        * Frontend up
    * Room has HDMI and VGA cables, if you do not have these bring your own dongle.
    * After your short 2-3 min presentation we will ask you some questions about the application or code.
    * We will discuss your points so look them over and have some questions
