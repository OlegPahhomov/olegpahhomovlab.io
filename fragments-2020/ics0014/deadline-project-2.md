| deadline      | date        | notes  |
| ------------- |-------------| -----|
| early bird | Tuesday, **16.03 23:59** | extra points |
| on time | Saturday, **20.03 23:59** | each hour late is -2% rounded upwards |
| late | Saturday, **27.03 23:59** | 1 week late -30% |
