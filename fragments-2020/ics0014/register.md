1) Registration opens after declaration deadline
1) Register in [excel](https://docs.google.com/spreadsheets/d/1GWRq68rbFM3CSwKyOGkfLbJNbRuMT2WpJ8y0lHmWRGw/edit?usp=sharing)
    * ``https://docs.google.com/spreadsheets/d/1GWRq68rbFM3CSwKyOGkfLbJNbRuMT2WpJ8y0lHmWRGw/edit?usp=sharing``
    * excel is generated from õis so register for the course
1) Create a PRIVATE channel (in the lecturer slack) with name team_number_teamname where number is your team number, teamname is your team name (team name is optional)
    1) For example: team_14_souroranges
    1) Important: teams < 10 use 0 before your number, so team_03_architects
        1) It helps with slack order (for me mostly)
1) Invite all your team members to that channel +
    1) @Oleg Pahhomov 
