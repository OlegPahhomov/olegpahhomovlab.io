| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird | Tuesday, **23.02 23:59** | extra points |
| on time | Saturday, **27.02 23:59** | each hour late is -2% rounded upwards |
| late | Saturday, **06.03 23:59** | 1 week late -30% |
