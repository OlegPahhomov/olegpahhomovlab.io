| deadline      | date        | notes  |
| ------------- |:-------------:| -----|
| early bird (migrating bird) | Saturday, 05.12.2020 23:59 | extra points |
| early bird | Saturday, 12.12.2020 23:59 | extra points |
| half-a-bird | Saturday, 19.12.2020 23:59 | 50% of extra points |
| on time | Saturday, 02.01.2021 23:59 | each hour late is -2% rounded upwards |
| late | Saturday, 09.01.2021 23:59 | 1 week late -30% |
