#### Building tool
* gitlab-ci  

**No other build tools are allowed**, real life alternatives are jenkins, bitbucket pipelines, bamboo
#### 1 server
As you noticed we are using 1 AWS EC2 for everything. 
While this works for small projects (like ours) it is usually separated as following:
* separate buildserver (gitlab runner)
* application server (back/front/nginx)
* database server (doesn't make sense for h2)
   * can be some instance of database as a service (DaaS)

#### Jar file and backend linux process
As an alternative you can use any combination of docker
* run docker file
* have docker-compose file which runs backend docker file
    * can run all other components as well
* can use some third party docker product

Real life use varies by companies.  
* Same configuration as ours.   
* Kubernetes/Rancher to manage Docker images.  
    * There are often multiple instances of same application running
* Cloud
    * AWS stack has multiple xyz-as-a-service platforms like cloudformation.

#### Monitoring
You can add some sort of basic monitoring. For backend install spring-actuator.
Monitoring would be in a separate server.
I heard that zabbix is easy to install.

In real life most popular would be graylog for logs and alerts and grafana for visualization. 
Most clouds provide some sort of version of events and alerts.
