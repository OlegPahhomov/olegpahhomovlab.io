| deadline      |            date             | notes  |
| ------------- |:---------------------------:| -----|
| early bird |  Saturday, **19.02 23:59**  | extra points |
| on time | Tuesday, **22.02 23:59**  | each hour late is -2% rounded upwards |
| late | Saturday, **26.02 23:59** | 1 week late -30% |
