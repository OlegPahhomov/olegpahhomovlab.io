1) Register in [excel](https://docs.google.com/spreadsheets/d/1EC2bHfK1J98Mg8E2ZB3utNzPH2zjRxPrcTQBr9OWrSI/edit#gid=0)
    * ``https://docs.google.com/spreadsheets/d/1EC2bHfK1J98Mg8E2ZB3utNzPH2zjRxPrcTQBr9OWrSI/edit#gid=0``
    * if your name is missing let me know, excel is generated from õis
2) Create a PRIVATE channel (in the lecturer slack) with name team_number_teamname where number is your team number, teamname is your team name (team name is optional)
    1) For example: team_14_souroranges
    1) Important: teams < 10 use 0 before your number, so team_03_architects
        1) It helps with slack order (for me mostly)
3) Invite all your team members to that channel +
    1) @Oleg Pahhomov 
