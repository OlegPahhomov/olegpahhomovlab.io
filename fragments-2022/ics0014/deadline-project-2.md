| deadline      | date                      | notes  |
| ------------- |---------------------------| -----|
| early bird | Tuesday, **08.03 23:59**  | extra points |
| on time | Saturday, **12.03 23:59** | each hour late is -2% rounded upwards |
| late | Tuesday, **15.03 23:59**   | -30% |
